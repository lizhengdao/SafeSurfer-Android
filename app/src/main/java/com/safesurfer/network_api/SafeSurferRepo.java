package com.safesurfer.network_api;

import com.safesurfer.network_api.entities.DeviceAuthResponse;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.network_api.entities.GetCastEventsResponse;
import com.safesurfer.network_api.entities.GetCastsListResponse;
import com.safesurfer.network_api.entities.LogInResponse;
import com.safesurfer.network_api.entities.SetBlockedSitesRequestBody;
import com.safesurfer.tensorflow.EventModelScores;

import org.json.JSONObject;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class SafeSurferRepo {

    private NetworkClient apiClient;

    public SafeSurferRepo(NetworkClient apiClient) {
        this.apiClient = apiClient;
    }

    public NetworkClient getApiClient() {
        return apiClient;
    }

    public Single<Response<ResponseBody>> createNewUser(String email, String password) {
        return getApiClient().createNewUser(email,password);
    }

    public Single<Response<LogInResponse>> logIn(String email, String password, boolean enableSub) {
        return getApiClient().logIn(email,password, enableSub);
    }

    public Single<Response<ResponseBody>> registerDevice(String jwtToken, String type, String name) {
        return getApiClient().registerDevice(jwtToken,type,name);
    }

    public Single<Response<DeviceAuthResponse>> deviceAuth(String jwtToken, String deviceID, String email, String password, boolean enableSub) {
        return getApiClient().deviceAuth(jwtToken, deviceID, email, password, enableSub );
    }

    public Single<Response<GetBlockedSitesResponse>> getBlockedSites(String jwtToken) {
        return getApiClient().getBlockedSites( jwtToken);
    }

    public Single<Response<String>> screencastStart(String jwtToken) {
        return getApiClient().screencastStart(jwtToken);
    }

    public Single<Response<String>> screencastStop(String jwtToken) {
        return getApiClient().screencastStop(jwtToken);
    }

    public Single<Response<String>> addCastEvent(String jwtToken, String castId, long timestamp, EventModelScores scores, String category, MultipartBody.Part image) {

        //  {"Explicit": 0.75, "Suggestive": 0.5, "Neutral": 0.25}.
        JSONObject levels = new JSONObject();
        try {
            levels.put("Explicit", scores.getExplicit()/100 );
            levels.put("Suggestive", scores.getSuggestive()/100 );
            levels.put("Neutral", scores.getNeutral()/100 );
        } catch (Exception e) {
            e.printStackTrace();
        }

        return getApiClient().addCastEvent(jwtToken, castId, timestamp, levels.toString() , category, image);
    }

    public Single<Response<GetCastEventsResponse>> getCastEvents(String jwtToken, String castId, String page, String num_per_page) {
        return getApiClient().getCastEventsList(jwtToken, castId, page, num_per_page);
    }

    public Single<Response<GetCastsListResponse>> getCastsList(String jwtToken, int page, int num_per_page, String device) {
        return getApiClient().getCastsList(jwtToken,page,num_per_page,device);
    }

    public Single<Response<String>> setBlockedSites(String jwtToken, SetBlockedSitesRequestBody body) {
        return getApiClient().setBlockedSites(jwtToken,body);
    }

    public Single<Response<ResponseBody>> requestCheck(String url) {
        return getApiClient().requestCheck( url);
    }

    public Single<Response<ResponseBody>> updateDeviceIp(String jwtToken, String deviceIp) {
        return getApiClient().updateDeviceIp(jwtToken, deviceIp);
    }

    public Single<Response<ResponseBody>> getScreencasts(String jwtToken, String page, String num_per_page, String device) {
        return getApiClient().getScreencasts(jwtToken, page, num_per_page, device);
    }

    public Single<Response<ResponseBody>> refreshJWTToken(String jwtToken) {
        return getApiClient().refreshJWTToken( jwtToken);
    }

}
