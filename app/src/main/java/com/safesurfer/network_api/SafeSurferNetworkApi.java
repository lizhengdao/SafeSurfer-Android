package com.safesurfer.network_api;

public class SafeSurferNetworkApi {

    private static SafeSurferNetworkApi mInstance;
    private NetworkClient apiClient;
    private SafeSurferRepo mSafeSurferRepo;
    private SafeSurferUsecase mSafeSurferUseCase;

    private SafeSurferNetworkApi(){
        apiClient = new RealNetworkClient();
        mSafeSurferRepo = new SafeSurferRepo(apiClient);
        mSafeSurferUseCase = new SafeSurferUsecase(mSafeSurferRepo);
    }

    public static SafeSurferNetworkApi getInstance(){
        if(mInstance == null)mInstance = new SafeSurferNetworkApi();
        return mInstance;
    }

    public SafeSurferUsecase getSRUseCase() {
        return mSafeSurferUseCase;
    }

}
