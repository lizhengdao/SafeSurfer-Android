package com.safesurfer.screens.detector_configuration;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.safesurfer.R;
import com.safesurfer.util.ThemeHandler;
import com.safesurfer.util.Tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.safesurfer.util.Tools.SCREENCAST_MODE_NONE;
import static com.safesurfer.util.Tools.SCREENCAST_MODE_WATCHING;

public class DetectorConfigurationActivity extends AppCompatActivity {

    Context context;

    TextView from_value;
    TextView to_value;

    AlertDialog dialogScreencastDetection;
//    Необходимо показывать текущий статус, как в диалоге при нажатии на элемент меню.
//    Кроме того необходимо задавать такие значения:
//
//    Временной интервал от и до для выбора случайного времени следующего сканирования экрана.
//    Доступны фиксированные значения:
//            - 1, 2, 5, 10, 20, 30, 45 - минут; 1, 1.5, 2, 3, 4, 5, 10 - часов
//    Предлагается использовать такой пикер-диалог https://take.ms/kBqxn
//    Начальный и конечный интервал задаются в списке, представляют из себя 2 последовательно расположенных элемента.
//          Always
//          Watching
//    Галочка для указания, нужно ли стартовать 'Always' при старте системы, если он был запущен до перезапуска.
//            - Restart 'Always' when device is started.


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setTheme(ThemeHandler.getPreferenceTheme(this));
        setContentView(R.layout.activity_detector_configuration);

        // clickable_dialog_from
        // clickable_dialog_to
        // from_value
        // to_value

        from_value = findViewById(R.id.from_value);
        to_value = findViewById(R.id.to_value);

        findViewById(R.id.clickable_dialog_from).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialogFrom();
            }
        });

        findViewById(R.id.clickable_dialog_to).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialogTo();
            }
        });


        from_value.setText(formatValue(Tools.getInstance().getValueFrom()));

        to_value.setText(formatValue(Tools.getInstance().getValueTo()));

        ((CheckBox) (findViewById(R.id.autostart))).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                Tools.getInstance().setAutostart(checked);
            }
        });

        ((CheckBox) (findViewById(R.id.autostart))).setChecked(Tools.getInstance().getAutostart() );

        if (Tools.getInstance().isScreencastDetectionActive()) {
            ((TextView) findViewById(R.id.textview_isscreenscast_active)).setText(context.getString(R.string.screencast_on));
            findViewById(R.id.textview_screenscast_mode).setVisibility(View.VISIBLE);
            switch (Tools.getInstance().getScreencastMode()) {
//                case SCREENCAST_MODE_ALWAYS:
//                    ((TextView) findViewById(R.id.textview_screenscast_mode)).setText(context.getString(R.string.screencast_always));
//                    break;
                case SCREENCAST_MODE_WATCHING:
                    ((TextView) findViewById(R.id.textview_screenscast_mode)).setText(context.getString(R.string.screencast_watching));
                    break;
                case SCREENCAST_MODE_NONE:
                    ((TextView) findViewById(R.id.textview_screenscast_mode)).setText("");
                    break;
            }
        } else {
            ((TextView) findViewById(R.id.textview_isscreenscast_active)).setText(context.getString(R.string.screencast_off));
            findViewById(R.id.textview_screenscast_mode).setVisibility(View.GONE);
        }
    }

    private String formatValue(int value) {
        String result = "";
        if (value < 60) {
            result = String.format("%s min", value);
        } else {
            if (((float) (value / 60)) < 2f) {
                result = String.format("%s hour", value / 60);
            } else {
                result = String.format("%s hours", value / 60);
            }
        }

        return result;
    }



    private void createDialogFrom() {

        int valueTo = Tools.getInstance().getValueTo();

        final int[] values = getResources().getIntArray(R.array.from_time_items_values);
        final String[] strings = getResources().getStringArray(R.array.from_time_items_array);

        dialogScreencastDetection = new AlertDialog.Builder(context)
                .setTitle(getResources().getString(R.string.screencast_detection_title))
                .setItems(strings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {

                        int value = values[position];
                        Tools.getInstance().saveValueFrom(value);

                        from_value.setText(strings[position]);

                        updateToValue();
                        dialogScreencastDetection.dismiss();
                    }
                }).create();

        dialogScreencastDetection.show();

    }

    private void updateToValue(){
        try {
            int valueFrom = Tools.getInstance().getValueFrom();

            final int[] values = getResources().getIntArray(R.array.to_time_items_values);

            int index = getIndex(valueFrom, values);

            if(index == -1){
                Tools.getInstance().saveValueTo(values[0]);

                String[] strings = getResources().getStringArray(R.array.to_time_items_array);
                to_value.setText( String.valueOf(strings[0]) );
            }else{
                Tools.getInstance().saveValueTo(values[index+1]);

                String[] strings = getResources().getStringArray(R.array.to_time_items_array);
                to_value.setText( String.valueOf(strings[index+1]) );
            }
        } catch ( Exception e) {
            e.printStackTrace();
        }
    }

    private void createDialogTo() {

        int valueFrom = Tools.getInstance().getValueFrom();

        final int[] values = getResources().getIntArray(R.array.to_time_items_values);

        final int[] trimmedValues = createTrimmedValuesTo(valueFrom, values);
        final String[] trimmedStrings = createTrimmedStringsTo(valueFrom, values);

        dialogScreencastDetection = new AlertDialog.Builder(context)
                .setTitle(getResources().getString(R.string.screencast_detection_title))
                .setItems(trimmedStrings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {

                        int valueFrom = Tools.getInstance().getValueFrom();

                        int valueTo = trimmedValues[position];

                        if (valueFrom < valueTo) {

                            Tools.getInstance().saveValueTo(valueTo);

                            to_value.setText(trimmedStrings[position]);

                            dialogScreencastDetection.dismiss();
                        }
                    }
                }).create();

        dialogScreencastDetection.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        Animatoo.animateSlideRight(context);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
            Animatoo.animateSlideRight(context);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*==============================================*/

    private int getIndex(int targetValue, int[] values ){
        int result = -1;
        int index = -1;
        for(int value : values){
            index++;
            if(targetValue == value){
                result = index;
                return result;
            }
        }
        return result;
    }

    private String[] createTrimmedStringsTo(int valueFrom, int[] values){

        int index = getIndex(valueFrom, values );

        String[] strings = getResources().getStringArray(R.array.to_time_items_array);
        List<String> list = Arrays.asList(strings);
        List<String> trimmedlist = list.subList(index+1, strings.length );

        String[] trimmedStrings = new String[trimmedlist.size() ];
        trimmedlist.toArray(trimmedStrings);
        return trimmedStrings;
    }

    private int[] createTrimmedValuesTo(int valueFrom, int[] values){

        int index = getIndex(valueFrom, values );

        List<Integer> list = new ArrayList<>();

        for(int value : values){
            list.add(value);
        }

        List<Integer> trimmedlist = list.subList(index+1, values.length );

        int[] trimmedValues = new int[trimmedlist.size() ];

        for(Integer value : trimmedlist){
            trimmedValues[trimmedlist.indexOf(value)] = value;
        }

        return trimmedValues;
    }

//    private String[] createTrimmedStringsFrom(int valueTo, int[] values){
//
//        int index = getIndex(valueTo, values );
//
//        String[] strings = getResources().getStringArray(R.array.time_items_array);
//        List<String> list = Arrays.asList(strings);
//        List<String> trimmedlist = list.subList(0, index );
//
//        String[] trimmedStrings = new String[trimmedlist.size() ];
//        trimmedlist.toArray(trimmedStrings);
//        return trimmedStrings;
//    }

//    private int[] createTrimmedValuesFrom(int valueTo, int[] values){
//
//        int index = getIndex(valueTo, values );
//
//        List<Integer> list = new ArrayList<>();
//
//        for(int value : values){
//            list.add(value);
//        }
//
//        List<Integer> trimmedlist = list.subList(0, index );
//
//        int[] trimmedValues = new int[trimmedlist.size() ];
//
//        for(Integer value : trimmedlist){
//            trimmedValues[trimmedlist.indexOf(value)] = value;
//        }
//
//        return trimmedValues;
//    }

}
