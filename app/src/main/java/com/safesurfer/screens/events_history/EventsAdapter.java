package com.safesurfer.screens.events_history;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.safesurfer.R;
import com.safesurfer.tensorflow.EventModel;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.OrderedRealmCollection;
import io.realm.RealmObject;
import io.realm.RealmRecyclerViewAdapter;

import static com.safesurfer.tensorflow.EventModel.EventModelTypesNEUTRAL;
import static com.safesurfer.tensorflow.EventModel.EventModelTypesEXPLICIT;
import static com.safesurfer.tensorflow.EventModel.EventModelTypesSUGGESTIVE;

public class EventsAdapter extends RealmRecyclerViewAdapter<RealmObject, EventsAdapter.EventsAdapterViewHolder> {

    private Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public EventsAdapter(@Nullable OrderedRealmCollection<RealmObject> data, boolean autoUpdate, boolean updateOnModification) {
        super(data, autoUpdate, updateOnModification);
    }

    public EventsAdapter(@Nullable OrderedRealmCollection<RealmObject> data, boolean autoUpdate) {
        super(data, autoUpdate);
    }

    @NonNull
    @Override
    public EventsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = ((AppCompatActivity) getContext()).getLayoutInflater().inflate(R.layout.settings_eventshistory_item, null);

        itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        return new EventsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EventsAdapterViewHolder holder, final int position) {

        EventModel model = (EventModel) getItem(position);

        holder.title.setText( createTitle(model) );
        holder.time.setText( formatDateTime(model) );

        holder.itemView.findViewById(R.id.container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getListener().onItemClick( (EventModel)getItem(position));
            }
        });

    }

    private String formatDateTime(EventModel model){

        long timestamp = model.getTimestamp();

        SimpleDateFormat formatter = new SimpleDateFormat(getContext().getString(R.string.datetime_scheme));
        String dateString = formatter.format(new Date( timestamp));

        return dateString;
    }

    private String createTitle(EventModel model) {
        int type = model.getEventModeltype();

        String title = "";

        switch (type) {
            case EventModelTypesNEUTRAL:
                title = getContext().getString(R.string.EventModelTypesNEUTRAL_title);
                break;
            case EventModelTypesEXPLICIT:
                title = getContext().getString(R.string.EventModelTypesEXPLICIT_title);
                break;
            case EventModelTypesSUGGESTIVE:
                title = getContext().getString(R.string.EventModelTypesSUGGESTIVE_title);
                break;
        }

        return title;
    }

    public static class EventsAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView time;

        public EventsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            time = itemView.findViewById(R.id.time);
        }
    }

    public EventsAdapterListener getListener() {
        return listener;
    }

    public void setListener(EventsAdapterListener listener) {
        this.listener = listener;
    }

    private EventsAdapterListener listener;

    public static interface EventsAdapterListener{
        public abstract void onItemClick(EventModel model );
    }
}
