package com.safesurfer.screens.events_history.events_history_item;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.safesurfer.R;
import com.safesurfer.tensorflow.EventModel;
import com.safesurfer.util.Tools;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.safesurfer.tensorflow.EventModel.EventModelTypesNEUTRAL;
import static com.safesurfer.tensorflow.EventModel.EventModelTypesEXPLICIT;
import static com.safesurfer.tensorflow.EventModel.EventModelTypesSUGGESTIVE;

public class EventsItemActivityVC {

    TextView date_time;
    TextView type_name;
    TextView type_threshold_value;
    TextView score_explicit;
    TextView score_suggestive;
    TextView score_neutral;
    ImageView screen_imageview;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private Context context;

    public EventsItemActivityVC(Context context) {
        setContext(context);
    }

    private View getRootView() {
        return ((AppCompatActivity) getContext()).findViewById(android.R.id.content);
    }

    public void setUpView() {
        date_time = getRootView().findViewById(R.id.date_time);
        type_name = getRootView().findViewById(R.id.type_name);
        type_threshold_value = getRootView().findViewById(R.id.type_threshold_value);
        score_explicit = getRootView().findViewById(R.id.score_explicit);
        score_suggestive = getRootView().findViewById(R.id.score_suggestive);
        score_neutral = getRootView().findViewById(R.id.score_neutral);
        screen_imageview = getRootView().findViewById(R.id.screen_imageview);
    }

    byte[] imageByteArray;

    public void populateView(int identifier) {
        try {
            EventModel model = getModel(identifier);
            if (model == null) return;

            String dateTime = formatDateTime(model);
            date_time.setText(dateTime);

            String modelTypeName = model.getEventModelTypeName();
            type_name.setText(modelTypeName);

            String thresholdString = createThresholdString(model);
            type_threshold_value.setText(thresholdString);

            score_explicit.setText(formatScore(model.getScores().getExplicit()));
            score_suggestive.setText(formatScore(model.getScores().getSuggestive()));
            score_neutral.setText(formatScore(model.getScores().getNeutral()));


            imageByteArray = model.getImageArray();
            if (imageByteArray != null) {
                singleThreadExecutor.submit(new Runnable() {
                    @Override
                    public void run() {

                        final Bitmap bitmap = BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length, new BitmapFactory.Options());

                        mUiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                screen_imageview.setVisibility(View.VISIBLE);
                                screen_imageview.setImageBitmap(bitmap);
                            }
                        });
                    }
                });
            } else {
                screen_imageview.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
    Handler mUiHandler = new Handler();

    private String formatScore(float value) {
        String scoreString = String.format("%s", (int) (value)) + " %";
        return scoreString;
    }

    private String createThresholdString(EventModel model) {
        String result = "";
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        int thresholdValue = 0;

        switch (model.getEventModeltype()) {
            case EventModelTypesNEUTRAL:
                thresholdValue = Tools.getInstance().getNeutralThreshold();
                break;
            case EventModelTypesEXPLICIT:
                thresholdValue = Tools.getInstance().getExplicitThreshold();
                break;
            case EventModelTypesSUGGESTIVE:
                thresholdValue = Tools.getInstance().getSuggestiveThreshold();
                break;
        }

        result = String.format("%s", thresholdValue) + " %";
        return result;
    }

    private EventModel getModel(int identifier) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<EventModel> list = realm.where(EventModel.class).equalTo("identifier", identifier).findAll();

        if (list.size() != 0) {
            EventModel model = list.get(0);
            return model;
        }
        return null;
    }

    //        Calendar calendar = Calendar.getInstance();
//        TimeZone tz = TimeZone.getDefault();
//        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//        java.util.Date currenTimeZone=new java.util.Date((long)1379487711*1000);

    // 01 Apr, 15:34:33

    private String formatDateTime(EventModel model) {
        String dateTime = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat(getContext().getString(R.string.datetime_scheme) );
            Log.d(getClass().getName(), " formatDateTime model.getTimestamp() " + String.valueOf(model.getTimestamp()) );
            dateTime = formatter.format(new Date( model.getTimestamp() ));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateTime;
    }

    private String createTitle(EventModel model) {
        int type = model.getEventModeltype();

        String title = "";

        switch (type) {
            case EventModelTypesNEUTRAL:
                title = "Neutral";
                break;
            case EventModelTypesEXPLICIT:
                title = "Explicit";
                break;
            case EventModelTypesSUGGESTIVE:
                title = "Suggestive";
                break;
        }

        return title;
    }

}
