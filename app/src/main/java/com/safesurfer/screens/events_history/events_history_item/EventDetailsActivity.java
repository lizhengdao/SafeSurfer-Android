package com.safesurfer.screens.events_history.events_history_item;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.safesurfer.R;
import com.safesurfer.util.ThemeHandler;

public class EventDetailsActivity extends AppCompatActivity {

    EventsItemActivityVC viewController;
    public static final String EventsHistorySettingsItemActivity_EVENTMODEL_id = "EventsHistorySettingsItemActivity_EVENTMODEL_id";

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setTheme(ThemeHandler.getPreferenceTheme(this));

        setContentView(R.layout.activity_events_history_settings_item);

        viewController = new EventsItemActivityVC(this);
        viewController.setUpView();

        viewController.populateView(getEventId() );
    }

    private int getEventId(){
        Bundle bundle = getIntent().getBundleExtra("extra");
        return bundle.getInt(EventsHistorySettingsItemActivity_EVENTMODEL_id, 0);
    }

    public static Intent getIntent(Context context, int identifier){

        Intent intent = new Intent(context, EventDetailsActivity.class );

        Bundle extra = new Bundle();
        extra.putInt(EventsHistorySettingsItemActivity_EVENTMODEL_id, identifier);

        intent.putExtra("extra",extra);

        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            Animatoo.animateSlideRight(context);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        Animatoo.animateSlideRight(context);
    }
}
