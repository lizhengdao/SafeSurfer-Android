package com.safesurfer.screens.events_history;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.network_api.entities.GetCastsListResponse;
import com.safesurfer.tensorflow.EventModel;
import com.safesurfer.util.Preferences;
import com.safesurfer.util.ThemeHandler;
import com.safesurfer.util.Tools;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 * <p>
 * This file is part of SafeSurfer-Android.
 * <p>
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class EventsListActivity extends AppCompatActivity {

    EventsListActivityVC viewController;

    Context context;

    @SuppressLint("CheckResult")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        context = this;
        setTheme(ThemeHandler.getPreferenceTheme(this));

        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_eventshistory_activity);

        viewController = new EventsListActivityVC(this);

        viewController.setUpView();

//        String deviceAuthToken = Tools.getInstance().getDeviceAuthToken();
//        if (deviceAuthToken != null) {
//            SafeSurferNetworkApi.getInstance()
//                    .getSRUseCase()
//                    .getCastsList(deviceAuthToken, 1, 100, "all")
//                    .toObservable()
//                    .subscribe(success -> {
//                        getEvents(success.body());
//                    }, fail -> {
//                        fail.printStackTrace();
//                    });
//        }
//        String currentCastId = Tools.getInstance().getCurrentScreencastId();
//        if (deviceAuthToken != null & currentCastId != null) {
//            SafeSurferNetworkApi.getInstance()
//                    .getSRUseCase()
//                    .getCastEvents(deviceAuthToken, currentCastId, "1", "100")
//                    .toObservable()
//                    .subscribe(success -> {
//                        Log.d(getClass().getName(), "getCastEvents() success");
//                    }, fail -> {
//                        fail.printStackTrace();
//                    });
//        }
    }

//    private void getEvents(GetCastsListResponse response) {
//        String deviceAuthToken = Tools.getInstance().getDeviceAuthToken();
//        for (GetCastsListResponse.Cast cast : response.casts) {
//            if (cast.numEvents.intValue() != 0) {
//                if (deviceAuthToken != null) {
//                    SafeSurferNetworkApi.getInstance()
//                            .getSRUseCase()
//                            .getCastEvents(deviceAuthToken, String.valueOf(cast.id), "1", "100")
//                            .toObservable()
//                            .subscribe(success -> {
//
//                            }, fail -> {
//                                fail.printStackTrace();
//                            });
//                }
//            }
//        }
//    }

    @Override
    public SharedPreferences getSharedPreferences(String name, int mode) {
        return Preferences.getInstance(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        Animatoo.animateSlideRight(context);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_eventslist, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
            Animatoo.animateSlideRight(context);
            return true;
        }

        if (item.getItemId() == R.id.delete_all) {
            createScreencastDetectionDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    AlertDialog dialogScreencastDetection;

    private void createScreencastDetectionDialog() {
        dialogScreencastDetection = new AlertDialog.Builder(this)
                .setTitle(context.getString(R.string.screencast_detection_removeall_title))
                .setNegativeButton(context.getString(R.string.screencast_detection_cancel_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogScreencastDetection.dismiss();
                    }
                })
                .setPositiveButton(context.getString(R.string.screencast_detection_removeall_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        removeAllEvents();
                        dialogScreencastDetection.dismiss();
                    }
                }).create();

        dialogScreencastDetection.show();
    }

    private void removeAllEvents() {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<EventModel> list = realm.where(EventModel.class).findAll();
                list.deleteAllFromRealm();
            }
        });
    }


}
