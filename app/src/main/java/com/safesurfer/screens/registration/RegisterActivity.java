package com.safesurfer.screens.registration;

/**
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;

import com.frostnerd.lifecycle.BaseActivity;

import com.safesurfer.R;
import com.safesurfer.util.Constants;

public class RegisterActivity extends BaseActivity {

    RegisterActivityViewController viewController;
    RegisterActivityDataProvider dataProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);

        viewController = new RegisterActivityViewController(this);
        dataProvider = new RegisterActivityDataProvider(this);
        //TODO: How to call the register activity?
        //met = findViewById(R.id.registerProgress);



        viewController.setListener(new RegisterActivityViewController.RegisterActivityViewControllerListener() {
            @Override
            public void makeRegisterRequest(String email, String password, String secureId, String mac) {
                dataProvider.requestRegister(email,password, secureId, mac);
            }
        });

        dataProvider.setListener(new RegisterActivityDataProvider.RegisterActivityDataProviderListener() {
            @Override
            public void onSuccess(String email, String password, String secureID, String mac) {
                viewController.onSuccess(email,password,secureID,mac);
            }

            @Override
            public void onError(String errorMessage) {
                viewController.showError(errorMessage);
            }
        });

        viewController.setUpView();
    }


    @Override
    protected BaseActivity.Configuration getConfiguration() {
        return BaseActivity.Configuration.withDefaults().setDismissFragmentsOnPause(true);
    }

}
