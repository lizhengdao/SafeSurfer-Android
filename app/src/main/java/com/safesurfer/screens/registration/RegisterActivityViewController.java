package com.safesurfer.screens.registration;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.safesurfer.BaseViewController;
import com.safesurfer.LogFactory;
import com.safesurfer.R;
import com.safesurfer.screens.MainActivity;
import com.safesurfer.util.Constants;
import com.safesurfer.util.Tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class RegisterActivityViewController extends BaseViewController {

    public static interface RegisterActivityViewControllerListener {
        public abstract void makeRegisterRequest(final String email, final String password, String secureId, String mac);
    }

    public RegisterActivityViewControllerListener getListener() {
        return listener;
    }

    public void setListener(RegisterActivityViewControllerListener listener) {
        this.listener = listener;
    }

    private RegisterActivityViewControllerListener listener;

    private ProgressBar progressBar = null;
    private static final String LOG_TAG = "[RegisterActivity]";


    public RegisterActivityViewController(Context context) {
        super(context);
    }

    // post the UDID and email address to the API...
/*                RequestQueue requestQueue = Volley.newRequestQueue(RegisterActivity.this);

                StringRequest stringRequest = new StringRequest(
                        Request.Method.POST,
                        "https://www.safesurfer.co.nz/api/1.0/registerAndroidDevice.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {

                                    // NOTE: Need to check for ["Device Registered"] which is the un-documented response we get back on success...

                                    if (response.equals("[\"Device Registered\"]")) {
                                        //LogFactory.writeMessage(this, LOG_TAG, response.toString());
                                        onSuccess(email, password, secureID, mac);
                                        return;
                                    }

                                    JSONObject json = new JSONObject(response);

                                    if (json.has("0")) {

                                        // SUCCESS...
                                        //Log.d("RegisterActivity", response.toString());
                                        onSuccess(email, password, secureID, mac);
                                    } else if (json.has("4")) {

                                        // AUTHENTICATION FAILED...
                                        //Log.d("RegisterActivity", response.toString());

                                        showError("The email address is registered to a SafeSurfer account but the supplied password is incorrect. Please try again or tap lost password.");

                                    } else if (json.has("5")) {

                                        //"The device is already registered to a SafeSurfer account.
                                        onSuccess(email, password, secureID, mac);
                                    } else {

                                        // OTHER ERRORS...
                                        //Log.d("RegisterActivity", response.toString());

                                        showError("An Error Ocurred During Registration");
                                    }


                                } catch (JSONException e) {

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                showError("An Error Occurred During Registration");
                            }
                        }) {


                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("safesurfer_secret", BuildConfig.SAFESURFER_API_SECRET);
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/x-www-form-urlencoded; charset=UTF-8";
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("email", email);
                        params.put("password", password);
                        params.put("deviceID", secureID);
                        params.put("mac", mac);
                        return params;
                    }


                };

                requestQueue.add(stringRequest);*/

    @Override
    public void setUpView() {
        progressBar = (ProgressBar) getRootView().findViewById(R.id.registerProgress);

        getRootView().findViewById(R.id.button_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (progressBar != null) {
                    progressBar.setVisibility(View.VISIBLE);
                }
                // grab the email address
                EditText editEmailAddress = (EditText) getRootView().findViewById(R.id.edit_email_address);
                final String email = editEmailAddress.getText().toString();

                // check for a valid email address, alert if one has not been entered...
                if ((email == null) || email.isEmpty()) {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.INVISIBLE);
                    }


                    createWarningDialog("Please enter your email address to register");
                    return;
                } else {
                    if (!isValidEmail(email)) {
                        if (progressBar != null) {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        createWarningDialog("Please enter valid email address to register");
                        return;
                    }
                }
                // grab the password and confirmation, checking they are the same before continuing...
                EditText editPassword = (EditText) getRootView().findViewById(R.id.edit_password);
                final String password = editPassword.getText().toString();

                if (password.trim().isEmpty()) {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                    createWarningDialog("Please enter your password");
                    return;
                }

                // Create a UUID for this device...
                final String secureID = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                final String mac = getMacAddr();
                getListener().makeRegisterRequest(email, password,secureID,mac );
            }
        });

        getRootView().findViewById(R.id.textview_lostpassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogFactory.writeMessage(getContext(), LOG_TAG, "onLostPassword");

                Intent intent = new Intent(Intent.ACTION_VIEW);
                // https://my.safesurfer.io/send-password-reset
//                intent.setData(Uri.parse("https://www.safesurfer.co.nz/my-account/lost-password"));
                intent.setData(Uri.parse( LOSTPASSWORD_URL ));
                getContext().startActivity(intent);
            }
        });
        getRootView().findViewById(R.id.button_skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MainActivity.class);
                getContext().startActivity(intent);
                Tools.getInstance().setDeviceAuthToken(null);
                ((AppCompatActivity) getContext()).finish();
            }
        });
    }

    public static final String LOSTPASSWORD_URL = "https://my.safesurfer.io/send-password-reset";

    @Override
    public View getRootView() {
        return ((AppCompatActivity) getContext()).findViewById(android.R.id.content);
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < macBytes.length; i++) {
                    sb.append(String.format("%02X%s", macBytes[i], (i < macBytes.length - 1) ? ":" : ""));
                }
                return sb.toString().toLowerCase();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return getMacAddress();
    }

    public static String loadFileAsString(String filePath) throws java.io.IOException {
        StringBuffer data = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            data.append(readData);
        }
        reader.close();
        return data.toString();
    }

    public static String getMacAddress() {
        try {
            return loadFileAsString("/sys/class/net/eth0/address").toUpperCase().substring(0, 17);
        } catch (Exception e) {
            e.printStackTrace();
            return "02:00:00:00:00:00";
        }
    }

    public void onSuccess(String email, String password, String secureID, String mac) {

        if (progressBar != null) {
            progressBar.setVisibility(View.INVISIBLE);
        }

        // Save UDID and email to SharedPreferences...
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.REGISTRATION_PREFERENCES_KEY_UDID, secureID);
        editor.putString(Constants.REGISTRATION_PREFERENCES_KEY_MACADDRESS, mac);
        editor.putString(Constants.REGISTRATION_PREFERENCES_KEY_EMAIL, email);
        editor.putString(Constants.REGISTRATION_PREFERENCES_KEY_PASSWORD, password);
        editor.apply();

        // TODO: Save UDID and email to a file where it will survive a reinstall...

        Intent intent = new Intent(getContext(), MainActivity.class);
        getContext().startActivity(intent);
        ((AppCompatActivity) getContext()).finish();
    }

    private void createWarningDialog(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(getContext());
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }

    public void showError(String message) {

        if (progressBar != null) {
            progressBar.setVisibility(View.INVISIBLE);
        }
        AlertDialog.Builder bld = new AlertDialog.Builder(getActivity());
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

}
