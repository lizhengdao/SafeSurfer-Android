package com.safesurfer.screens.registration.agree_terms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.safesurfer.R;
import com.safesurfer.screens.registration.RegisterActivity;
import com.safesurfer.util.Tools;

public class AgreeTermsActivity extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        context = this;
        setContentView(R.layout.activity_agree_terms);

        WebView wv = (WebView) findViewById(R.id.webview);
        wv.loadUrl("file:///android_asset/welcome_en.html");



        findViewById(R.id.button_get_started).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.getInstance().setUserAgreeTerms(true);

                Intent RegisterIntent = new Intent(context, RegisterActivity.class);
                startActivity(RegisterIntent);
                finish();
            }
        });
        ((CheckBox) findViewById(R.id.checkbox_i_agree)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                findViewById(R.id.button_get_started).setEnabled(isChecked);
            }
        });

        findViewById(R.id.button_get_started).setEnabled(false);

    }
}
