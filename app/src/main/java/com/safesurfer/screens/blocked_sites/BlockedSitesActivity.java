package com.safesurfer.screens.blocked_sites;

/**
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */

import androidx.appcompat.app.AppCompatActivity;
import com.safesurfer.R;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.util.Constants;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.net.URLEncoder;

public class BlockedSitesActivity extends AppCompatActivity {
//TODO: when i click on to the blocksites activity it says "Sorry, an error occured - No user registered to this device" so i need to work on the registration and login for checking this issue
    private ProgressBar progressBar = null;

//    BlockedSitesActivityDataProvider dataProvider;
    BlockedSitesActivityViewController viewController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();

        setContentView(R.layout.activity_blocked_sites);

//        dataProvider = new BlockedSitesActivityDataProvider(this);
        viewController = new BlockedSitesActivityViewController(this);

        viewController.setUpView();
    }

    public void updateView(){
        viewController.updateView();
    }
}

//        try {
//
//            SharedPreferences preferences = getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
//            final String email = preferences.getString(Constants.REGISTRATION_PREFERENCES_KEY_EMAIL, null);
//            final String uuid = preferences.getString(Constants.REGISTRATION_PREFERENCES_KEY_UDID, null);
//            final String password = preferences.getString(Constants.REGISTRATION_PREFERENCES_KEY_PASSWORD, null);
//
//            progressBar = (ProgressBar) findViewById(R.id.webViewProgress);
//
//            WebView webView = (WebView) this.findViewById(R.id.webView);
//
//            WebSettings webSettings = webView.getSettings();
//            webSettings.setJavaScriptEnabled(true);
//            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//
//
//            webView.setWebViewClient(new WebViewClient()
//            {
//                @Override
//                public void onPageFinished (WebView view, String url) {
//
//                    if (progressBar != null) {
//
//                        progressBar.setVisibility(View.GONE);
//                    }
//
//                }
//
//                @Override
//                public boolean shouldOverrideUrlLoading(WebView view, String url)
//                {
//                    // we have to provide a web client so we can override this so that the redirection is handled in the webview rather than the onboard browser...
//                    return false;
//                }
//            });
//
//            byte[] data = String.format("email=%s&password=%s&deviceID=%s",
//                    URLEncoder.encode(email, "UTF-8"),
//                    URLEncoder.encode(password, "UTF-8"),
//                    URLEncoder.encode(uuid, "UTF-8")).getBytes();
//
//            webView.postUrl("https://www.safesurfer.co.nz/api/1.0/androidManageDevices.php", data);
//
//        }
//        catch (Exception ex) {
//
//            Log.d( "BlockedSitesActivity", ex.getMessage() );
//
//        }
