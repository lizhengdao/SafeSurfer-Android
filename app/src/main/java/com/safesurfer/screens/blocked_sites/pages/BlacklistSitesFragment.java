package com.safesurfer.screens.blocked_sites.pages;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.safesurfer.R;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.screens.blocked_sites.BlockedSitesActivity;

public class BlacklistSitesFragment extends Fragment {

    public static final int BlacklistSitesFragment_WHITELIST = 111;
    public static final int BlacklistSitesFragment_BLACKLIST = 222;
    public static final String BlacklistSitesFragment_TYPE = "BlacklistSitesFragment_TYPE";

    BlacklistSitesFragmentDP dataProvider;
    BlacklistSitesFragmentVC viewController;

    public static BlacklistSitesFragment getInstance(int type) {
        BlacklistSitesFragment fragment = new BlacklistSitesFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(BlacklistSitesFragment_TYPE, type);

        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataProvider = new BlacklistSitesFragmentDP(getActivity());
        viewController = new BlacklistSitesFragmentVC(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blacklist_sites, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int type = getArguments().getInt(BlacklistSitesFragment_TYPE, -100 );
        viewController.setUpView(type);

        viewController.setListener(new BlacklistSitesFragmentVC.BlacklistSitesFragmentVCListener() {
            @Override
            public void requestAddSiteToWhitelist(GetBlockedSitesResponse blockedSitesResponse, String url) {
                dataProvider.addSiteToWhitelist(blockedSitesResponse, url);
            }

            @Override
            public void requestAddSiteToBlacklist(GetBlockedSitesResponse blockedSitesResponse, String url) {
                dataProvider.addSiteToBlacklist(blockedSitesResponse,url);
            }
        });

        dataProvider.setListener(new BlacklistSitesFragmentDP.BlacklistSitesFragmentDPListener() {
            @Override
            public void blockedSiteAddedSuccessfully() {

                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
//                        int type = getArguments().getInt(BlacklistSitesFragment_TYPE, -100 );
//                        viewController.setUpView(type);
//                        dataProvider.getBlockedSites();
                        ((BlockedSitesActivity)getActivity()).updateView();
                    }
                });
            }

            @Override
            public void sitesListsReady(GetBlockedSitesResponse blockedSitesResponse) {
                viewController.sitesListsReady(blockedSitesResponse);
            }

            @Override
            public void requestError() {
                viewController.requestError();
            }
        });

        dataProvider.getBlockedSites();
    }

    public void onButtonAddClick(){
        viewController.onButtonAddClick();
    }

    Handler mUiHandler = new Handler();
}
