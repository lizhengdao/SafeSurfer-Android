package com.safesurfer.screens.blocked_sites;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.safesurfer.BaseViewController;
import com.safesurfer.R;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.screens.blocked_sites.pages.BlacklistSitesFragment;

import java.util.ArrayList;

import static com.safesurfer.screens.blocked_sites.pages.BlacklistSitesFragment.BlacklistSitesFragment_BLACKLIST;
import static com.safesurfer.screens.blocked_sites.pages.BlacklistSitesFragment.BlacklistSitesFragment_WHITELIST;

public class BlockedSitesActivityViewController extends BaseViewController {

    public BlockedSitesActivityViewController(Context context) {
        super(context);
    }

    RecyclerView recyclerview_blacklist;
    RecyclerView recyclerview_whitelist;
    SitesAdapter blackListAdapter;
    SitesAdapter whiteListAdapter;

    LinearLayoutManager llmanager0;
    LinearLayoutManager llmanager1;

    public void requestError() {
        getRootView().findViewById(R.id.registerProgress).setVisibility(View.GONE);
        getRootView().findViewById(R.id.container).setVisibility(View.GONE);

        AlertDialog.Builder bld = new AlertDialog.Builder(getActivity());
        bld.setMessage(R.string.string_network_error);
        bld.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().finish();
            }
        });
        bld.create().show();
    }

    public void sitesListsReady(GetBlockedSitesResponse blockedSitesResponse) {
////        recyclerview_blacklist.setAdapter(blackListAdapter);
////        recyclerview_whitelist.setAdapter(whiteListAdapter);
//        getRootView().findViewById(R.id.registerProgress).setVisibility(View.GONE);
//        getRootView().findViewById(R.id.container).setVisibility(View.VISIBLE);
//
//        for(String site: blockedSitesResponse.blacklist){
//            blackListAdapter.getSitesList().add(site);
//        }
//        blackListAdapter.notifyDataSetChanged();
//
//        for(String site: blockedSitesResponse.whitelist){
//            whiteListAdapter.getSitesList().add(site);
//        }
//        whiteListAdapter.notifyDataSetChanged();
//
//        if(blockedSitesResponse.whitelist.size() == 0){
//            getRootView().findViewById(R.id.recyclerview_whitelist_container).setVisibility(View.GONE);
//        }
//        if(blockedSitesResponse.blacklist.size() == 0){
//            getRootView().findViewById(R.id.recyclerview_blacklist_container).setVisibility(View.GONE);
//        }

    }

    TabLayout tab_layout;
    ViewPager pages_viewpager;
    ViewpagerFragmentAdapter fragmentsAdapter;

    @Override
    public void setUpView() {
        getRootView().findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        tab_layout = (TabLayout) getRootView().findViewById(R.id.tab_layout);
        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                pages_viewpager.setCurrentItem( tab_layout.getSelectedTabPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        pages_viewpager = getRootView().findViewById(R.id.pages_viewpager);

        fragmentsAdapter = new ViewpagerFragmentAdapter(getActivity().getSupportFragmentManager());


        fragmentsAdapter.addItem(BlacklistSitesFragment.getInstance(BlacklistSitesFragment_BLACKLIST));
        fragmentsAdapter.addItem(BlacklistSitesFragment.getInstance(BlacklistSitesFragment_WHITELIST));

        pages_viewpager.setAdapter(fragmentsAdapter);

        pages_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tab_layout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        pages_viewpager.setCurrentItem(0);
        tab_layout.getTabAt(0).select();

        getActivity().findViewById(R.id.button_add_site).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BlacklistSitesFragment)fragmentsAdapter.getItem(tab_layout.getSelectedTabPosition() )).onButtonAddClick();
            }
        });
    }

    public void updateView(){

        fragmentsAdapter = new ViewpagerFragmentAdapter(getActivity().getSupportFragmentManager());

        fragmentsAdapter.addItem(BlacklistSitesFragment.getInstance(BlacklistSitesFragment_BLACKLIST));
        fragmentsAdapter.addItem(BlacklistSitesFragment.getInstance(BlacklistSitesFragment_WHITELIST));

        pages_viewpager.setAdapter(fragmentsAdapter);

        pages_viewpager.setCurrentItem(tab_layout.getSelectedTabPosition() );
    }

    @Override
    public View getRootView() {
        return getActivity().findViewById(android.R.id.content);
    }

    public static class SiteViewHolder extends RecyclerView.ViewHolder {

        TextView textview;

        public SiteViewHolder(@NonNull View itemView) {
            super(itemView);
            textview = itemView.findViewById(R.id.title);
        }
    }
}
