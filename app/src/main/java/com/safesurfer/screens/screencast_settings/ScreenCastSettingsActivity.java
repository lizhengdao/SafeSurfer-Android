package com.safesurfer.screens.screencast_settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.safesurfer.R;
import com.safesurfer.screens.screencast_settings.presets.ScreenCastPresetsActivity;
import com.safesurfer.util.Preferences;
import com.safesurfer.util.ThemeHandler;
import com.safesurfer.util.Tools;

import static com.safesurfer.util.Tools.DEFAULT_LEVEL_EXPLICIT;
import static com.safesurfer.util.Tools.DEFAULT_LEVEL_NEUTRAL;
import static com.safesurfer.util.Tools.DEFAULT_LEVEL_SUGGESTIVE;
import static com.safesurfer.util.Tools.LEVEL_EXPLICIT;
import static com.safesurfer.util.Tools.LEVEL_NEUTRAL;
import static com.safesurfer.util.Tools.LEVEL_SUGGESTIVE;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class ScreenCastSettingsActivity extends AppCompatActivity {

    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeHandler.getPreferenceTheme(this));
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.detector_settings_activity);


        textview_level_neutral = findViewById(R.id.textview_level_neutral);
        textview_level_suggestive = findViewById(R.id.textview_level_suggestive);
        textview_level_explicit = findViewById(R.id.textview_level_explicit);

        seekbar_level_neutral = findViewById(R.id.seekbar_level_neutral);
        seekbar_level_suggestive = findViewById(R.id.seekbar_level_suggestive);
        seekbar_level_explicit = findViewById(R.id.seekbar_level_explicit);

        seekbar_level_neutral.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textview_level_neutral.setText("Neutral "+String.valueOf(seekBar.getProgress()) +"%" );
                Tools.getInstance().saveCastSettingValue(LEVEL_NEUTRAL, seekBar.getProgress());
            }
        });


        seekbar_level_suggestive.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textview_level_suggestive.setText("Suggestive "+String.valueOf(seekBar.getProgress()) +"%" );
                Tools.getInstance().saveCastSettingValue(LEVEL_SUGGESTIVE, seekBar.getProgress());
            }
        });

        seekbar_level_explicit.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textview_level_explicit.setText("Explicit "+String.valueOf(seekBar.getProgress()) +"%" );
                Tools.getInstance().saveCastSettingValue(LEVEL_EXPLICIT, seekBar.getProgress());
            }
        });

        findViewById(R.id.presets_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Tools.getInstance().getContext(), ScreenCastPresetsActivity.class);
                startActivity(intent);
                Animatoo.animateSlideLeft( context);
            }
        });
    }

    TextView textview_level_neutral;
    TextView textview_level_suggestive;
    TextView textview_level_explicit;

    SeekBar seekbar_level_neutral;
    SeekBar seekbar_level_suggestive;
    SeekBar seekbar_level_explicit;
    // level_neutral level_suggestive level_explicit


    @Override
    protected void onResume() {
        super.onResume();

        textview_level_neutral.setText("Neutral " + Tools.getInstance().getCastSettingValue(LEVEL_NEUTRAL, DEFAULT_LEVEL_NEUTRAL) + " %");
        textview_level_suggestive.setText("Suggestive " + Tools.getInstance().getCastSettingValue(LEVEL_SUGGESTIVE, DEFAULT_LEVEL_SUGGESTIVE) + " %");
        textview_level_explicit.setText("Explicit " + Tools.getInstance().getCastSettingValue(LEVEL_EXPLICIT, DEFAULT_LEVEL_EXPLICIT) + " %");

        seekbar_level_neutral.setProgress( Tools.getInstance().getCastSettingValue(LEVEL_NEUTRAL, DEFAULT_LEVEL_NEUTRAL));
        seekbar_level_suggestive.setProgress( Tools.getInstance().getCastSettingValue(LEVEL_SUGGESTIVE, DEFAULT_LEVEL_SUGGESTIVE));
        seekbar_level_explicit.setProgress( Tools.getInstance().getCastSettingValue(LEVEL_EXPLICIT, DEFAULT_LEVEL_EXPLICIT) );
    }

    @Override
    public SharedPreferences getSharedPreferences(String name, int mode) {
        return Preferences.getInstance(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        Animatoo.animateSlideRight(context);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            Animatoo.animateSlideRight(context);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
