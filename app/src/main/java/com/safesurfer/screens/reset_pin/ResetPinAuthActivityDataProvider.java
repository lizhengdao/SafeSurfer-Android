package com.safesurfer.screens.reset_pin;

import android.content.Context;
import android.util.Log;

import com.safesurfer.BaseDataProvider;
import com.safesurfer.network_api.SafeSurferNetworkApi;

public class ResetPinAuthActivityDataProvider extends BaseDataProvider {

    public ResetPinAuthActivityDataProvider(Context context) {
        super(context);
    }

    public static interface ResetPinAuthActivityDataProviderListener {
        public abstract void onAuthenticateSuccess();

        public abstract void onAuthenticateFail();
    }

    public ResetPinAuthActivityDataProviderListener getListener() {
        return listener;
    }

    public void setListener(ResetPinAuthActivityDataProviderListener listener) {
        this.listener = listener;
    }

    private ResetPinAuthActivityDataProviderListener listener;

    public static final String REGISTER_RESPONSE_SUCCESS = "\"success\"";
    public static final String REGISTER_RESPONSE_DUPLICATE = "\"duplicate\"";
    public static final String REGISTER_RESPONSE_ERROR = "\"error\"";

    public void userAuthenticate(String email, String password) {
        SafeSurferNetworkApi.getInstance()
                .getSRUseCase()
                .createNewUser(email, password)
                .toObservable()
                .subscribe(success -> {

                            String message;
                            Log.d(getClass().getName(), String.format("code %s", success.code()));
                            switch (success.code()) {
                                case 200:
                                    message = success.message();
                                    Log.d(getClass().getName(), message);

                                    if (message.equals(REGISTER_RESPONSE_SUCCESS)) {
                                        login(email, password);
                                    }
                                    break;
                                case 400:
                                    message = success.errorBody().string();
                                    if (message.equals(REGISTER_RESPONSE_DUPLICATE)) {
                                        login(email, password);
                                    }
                                    if (message.equals(REGISTER_RESPONSE_ERROR)) {
                                        getListener().onAuthenticateFail();
                                    }
                                    break;

                                default:
                                    getListener().onAuthenticateFail();
                                    break;
                            }

                        },
                        fail -> {
                        });
    }

    public void login(final String email, final String password) {

        SafeSurferNetworkApi.getInstance().getSRUseCase()
                .logIn(email, password, true)
                .toObservable()
                .subscribe(success -> {

                    switch (success.code()) {
                        case 200:
                            getListener().onAuthenticateSuccess();
                            break;
                        default:
                            getListener().onAuthenticateFail();
                            break;
                    }
                }, fail -> {
                    fail.printStackTrace();
                });
    }

}
