package com.safesurfer.util.libscreenshotter.scheduler;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.safesurfer.R;
import com.safesurfer.screens.EnterPinActivity;
import com.safesurfer.screens.MainActivity;
import com.safesurfer.util.Tools;
import com.safesurfer.util.libscreenshotter.Screenshotter;
import com.safesurfer.util.libscreenshotter.ScreenshotterDatabase;

import static com.safesurfer.util.Tools.FOREGROUND_NOTIFICATION_ID;

public class ScreenCapturerService extends Service {

    public static boolean isServiceRunning() {
        return serviceRunning;
    }

    public static void setServiceRunning(boolean serviceRunning) {
        ScreenCapturerService.serviceRunning = serviceRunning;
    }

    private static boolean serviceRunning;

    @Override
    public void onCreate() {
        super.onCreate();

        setServiceRunning(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(getClass().getName(), "onDestroy");

        stopForeground(true);

        setServiceRunning(false);
        Screenshotter.getInstance().tearDown();
        Screenshotter.getInstance().setListener(null);

        try {
            windowManager.removeViewImmediate(serviceDebugView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            windowManager.removeViewImmediate(serviceView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    View serviceView;
    WindowManager windowManager;

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        Log.d(getClass().getName(), "onActivityResult");
        createForegroundNotification();

        Screenshotter.getInstance()
                .setSize(720, 1280)
                .takeScreenshot(Tools.getInstance().getContext(), resultCode, data);

        if(Tools.getInstance().getUseBlackout()){
            try {
                useScreenBlackout();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(Tools.getInstance().getScreencastDebugMode()){
            try {
                useScreenDebugInfo();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void createForegroundNotification(){
        NotificationManager notificationManager = (NotificationManager) Tools.getInstance().getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Tools.getInstance().createNotificationChannel(notificationManager);


        Intent intent = new Intent(Tools.getInstance().getContext(), EnterPinActivity.class );
//        intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );

        PendingIntent notifyPendingIntent = PendingIntent.getActivity( Tools.getInstance().getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(Tools.getInstance().getContext(), Tools.getInstance().getNotificationChannelId() )
                .setSmallIcon(R.drawable.ic_stat_name)
                .setColor(Color.parseColor("#00A7D6"))
                .setContentTitle("Safe surfer")
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContentIntent(notifyPendingIntent)
                .setVibrate(new long[]{0L})
                .setContentText("Screencast detection");

//        notificationManager.notify(NOTIFICATION_ID, builder.build());

        startForeground(FOREGROUND_NOTIFICATION_ID, builder.build());
    }


    View serviceDebugView;

    private void useScreenDebugInfo(){
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        serviceDebugView = (View) inflater.inflate(R.layout.screen_debug_info, null);

        WindowManager.LayoutParams params;
        if(Tools.getSdkVersion() >= 26){
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | /*WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | */WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }else{
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }

        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;

        params.gravity = Gravity.TOP;

        windowManager.addView(serviceDebugView, params);

        Screenshotter.getInstance().setDebugModeListener(new Screenshotter.DebugModeListener() {
            @Override
            public void bitmapAnalyzed(final float neutral, final float suggestive, final float explicit) {
                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        String display = String.format("neutral %s \nsuggestive %s\nexplicit %s",neutral, suggestive, explicit);

                        Log.d(getClass().getName(), display);
                        ((TextView)serviceDebugView.findViewById(R.id.textview_display)).setText(display);
                    }
                });
            }
        });
    }

    private void useScreenBlackout(){
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        serviceView = (View) inflater.inflate(R.layout.screen_blackout, null);


        WindowManager.LayoutParams params;
        if(Tools.getSdkVersion() >= 26){
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    PixelFormat.TRANSLUCENT);
        }else{
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    PixelFormat.TRANSLUCENT);
        }

        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        windowManager.addView(serviceView, params);

        Screenshotter.getInstance().setListener(new ScreenshotterDatabase.ScreenshotterDatabaseListener() {
            @Override
            public void onBitmapIsSuggestive() {
                if(viewCanBeAnimated){
                    mUiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Tools.getInstance().viewFadeInOut( serviceView  );
                        }
                    });
                    viewCanBeAnimated = false;
                    animateViewTimer();
                }

            }

            @Override
            public void onBitmapIsExplicit() {
                if(viewCanBeAnimated){
                    mUiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Tools.getInstance().viewFadeInOut( serviceView  );
                        }
                    });
                    viewCanBeAnimated = false;
                    animateViewTimer();
                }
            }

            @Override
            public void onBitmapAnalyzed() {

            }

            @Override
            public void bitmapAnalyzed(float neutral, float suggestive, float explicit) {

            }
        });
    }

    boolean viewCanBeAnimated = true;
    Handler mUiHandler = new Handler();
    Runnable runnableViewCanBeAnimated;

    private void animateViewTimer(){

        if(runnableViewCanBeAnimated != null){
            mUiHandler.removeCallbacks(runnableViewCanBeAnimated);
        }
        runnableViewCanBeAnimated = new Runnable() {
            @Override
            public void run() {
                viewCanBeAnimated = true;
            }
        };
        mUiHandler.postDelayed(runnableViewCanBeAnimated, 5 * 1000);
    }

    @Override
    public IBinder onBind(@NonNull Intent intent) {

        ScreenCapturerServiceBinder binder = new ScreenCapturerServiceBinder();
        binder.setmServiceInstance(this);

        return binder;
    }


    public static class ScreenCapturerServiceBinder extends Binder {


        public ScreenCapturerService getmServiceInstance() {
            return mServiceInstance;
        }

        public void setmServiceInstance(ScreenCapturerService mServiceInstance) {
            this.mServiceInstance = mServiceInstance;
        }

        ScreenCapturerService mServiceInstance;

        public ScreenCapturerServiceBinder() {
            super();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        if (Tools.getInstance().isScreencastDetectionActive()) {
            Intent intent = new Intent(this, ScreenCapturerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

}
