package com.safesurfer.util.dnsquery;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import de.measite.minidns.AbstractDNSClient;
import de.measite.minidns.DNSClient;
import de.measite.minidns.DNSMessage;
import de.measite.minidns.Question;
import de.measite.minidns.Record;
import de.measite.minidns.record.Data;
import lombok.AccessLevel;
import lombok.Getter;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class Resolver {
    @Getter(lazy = true, value = AccessLevel.PRIVATE) private final AbstractDNSClient resolver = createResolver();
    @Getter(lazy = true, value = AccessLevel.PRIVATE) private final AbstractDNSClient tcpResolver = createTCPResolver();
    private InetAddress upstreamServer;
    private int timeout = -1;

    public Resolver(@NonNull String upstreamServer, @IntRange(from = 0) int timeout){
        setUpstreamAddress(upstreamServer);
        setTimeout(timeout);
    }

    public Resolver(@NonNull String upstreamServer){
        setUpstreamAddress(upstreamServer);
    }

    public void setUpstreamAddress(@NonNull String upstreamAddress) {
        try {
            upstreamServer = InetAddress.getByName(upstreamAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void setTimeout(@IntRange(from = 0) int timeout){
        if(resolver != null) getResolver().getDataSource().setTimeout(timeout);
        if(tcpResolver != null) getTcpResolver().getDataSource().setTimeout(timeout);
        this.timeout = timeout;
    }

    @NonNull
    public final <D extends Data> ResolverResult<D> resolve(@NonNull String name, @NonNull Record.TYPE type, @IntRange(from = 1, to = 65535) int port) throws IOException {
        return resolve(name, type, false, port);
    }

    @NonNull
    public final <D extends Data> ResolverResult<D> resolve(@NonNull String name, @NonNull Record.TYPE type, @NonNull Record.CLASS clazz, @IntRange(from = 1, to = 65535) int port) throws IOException {
        return resolve(name, type, clazz, false, port);
    }

    @NonNull
    public final <D extends Data> ResolverResult<D> resolve(@NonNull String name, @NonNull Record.TYPE type, boolean tcp, @IntRange(from = 1, to = 65535) int port) throws IOException {
        return resolve(name, type, Record.CLASS.IN, tcp, port);
    }

    @NonNull
    public final <D extends Data> ResolverResult<D> resolve(@NonNull String name, @NonNull Record.TYPE type, @NonNull Record.CLASS clazz, boolean tcp, @IntRange(from = 1, to = 65535) int port) throws IOException {
        Question q = new Question(name, type, clazz);
        return resolve(q, tcp, port);
    }

    @NonNull
    public final <D extends Data> ResolverResult<D> resolve(@NonNull String name, @NonNull Record.TYPE type) throws IOException {
        return resolve(name, type, false);
    }

    @NonNull
    public final <D extends Data> ResolverResult<D> resolve(@NonNull String name, @NonNull Record.TYPE type, @NonNull Record.CLASS clazz) throws IOException {
        return resolve(name, type, clazz, false);
    }

    @NonNull
    public final <D extends Data> ResolverResult<D> resolve(@NonNull String name, @NonNull Record.TYPE type, boolean tcp) throws IOException {
        return resolve(name, type, Record.CLASS.IN, tcp);
    }

    @NonNull
    public final <D extends Data> ResolverResult<D> resolve(@NonNull String name, @NonNull Record.TYPE type, @NonNull Record.CLASS clazz, boolean tcp) throws IOException {
        return resolve(name, type, clazz, tcp, 53);
    }

    @NonNull
    public <D extends Data> ResolverResult<D> resolve(@NonNull Question question, boolean tcp, @IntRange(from = 1, to = 65535) int port) throws IOException {
        DNSMessage dnsMessage;
        System.out.println("Sending question " + question + " to " + upstreamServer + ":" + port + " (tcp: " + tcp + ")");
        AbstractDNSClient resolver = tcp ? getTcpResolver() : getResolver();
        dnsMessage = resolver.query(question, upstreamServer, port);
        return new ResolverResult<>(question, dnsMessage, null);
    }

    @NonNull
    private AbstractDNSClient createResolver(){
        DNSClient client = new DNSClient();
        if(timeout != -1)client.getDataSource().setTimeout(timeout);
        return client;
    }

    @NonNull
    private AbstractDNSClient createTCPResolver(){
        DNSClient client = new DNSClient();
        client.setDataSource(new TCPDataSource());
        if(timeout != -1)client.getDataSource().setTimeout(timeout);
        return client;
    }

}
