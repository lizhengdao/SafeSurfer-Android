package com.safesurfer.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


import androidx.appcompat.app.AppCompatActivity;

import com.safesurfer.util.libscreenshotter.scheduler.ScreencastServiceScheduler;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;
import static com.safesurfer.tensorflow.TestTensorFlowRecognitionActivity.SCREENCAPTURER_SERVICE_ID;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_DEVICEID;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_SCREENCAST_ID;

public class Tools {

    private Context context;

    private Tools(){}
    private static Tools mInstance;

    public static final Tools getInstance(){
        if(mInstance == null)mInstance = new Tools();
        return mInstance;
    }

    public void init(Context context){
        setContext(context);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

//    public void scheduleJob() {
//        try {
//            cancelSheduleJob();
//            Log.d(getClass().getName(), "Job is scheduled");
//            ComponentName jobService = new ComponentName(getContext() , .class);
//            JobInfo.Builder exerciseJobBuilder = new JobInfo.Builder(SCREENCAPTURER_SERVICE_ID, jobService);
//
////            exerciseJobBuilder.setMinimumLatency(TimeUnit.SECONDS.toMillis(60));
//
//            exerciseJobBuilder.setMinimumLatency(0);
//            exerciseJobBuilder.setPeriodic( TimeUnit.SECONDS.toMillis(30));
//
//            exerciseJobBuilder.setRequiresDeviceIdle(false);
//            exerciseJobBuilder.setRequiresCharging(false);
//
//            Log.d(getClass().getName(), "scheduleJob: adding job to scheduler");
//
//            JobScheduler jobScheduler = (JobScheduler) getContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);
//            jobScheduler.schedule(exerciseJobBuilder.build());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void cancelSheduleJob(){
//        JobScheduler jobScheduler = (JobScheduler) getContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        jobScheduler.cancel(SCREENCAPTURER_SERVICE_ID);
//    }

    public static final String ScreencastDetectionACTIVE_KEY = "ScreencastDetectionACTIVE_KEY";

    public boolean isScreencastDetectionActive(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        return sharedPref.getBoolean(ScreencastDetectionACTIVE_KEY, false);
    }

    public boolean isScreencastDetectionNotificationsActive(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        return sharedPref.getBoolean("content_detector_notifications", false);
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void setScreencastDetectionActive(boolean active){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(ScreencastDetectionACTIVE_KEY, active);
        editor.commit();
    }



    public int getCastSettingValue(String key, int defaultValue){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        return prefs.getInt(key, defaultValue);
    }

    public void saveCastSettingValue(String key, int value){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static final String AUTOSTART_IS_ENABLED = "AUTOSTART_IS_ENABLED";

    public void setAutostart(boolean enabled){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(AUTOSTART_IS_ENABLED, enabled);

        editor.commit();
    }

    public boolean getAutostart(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getBoolean(AUTOSTART_IS_ENABLED, false);
    }

    public static final String SCREENCAST_MODE = "SCREENCAST_MODE";
//    public static final int SCREENCAST_MODE_ALWAYS = 111;
    public static final int SCREENCAST_MODE_WATCHING = 222;
    public static final int SCREENCAST_MODE_NONE = 0;

    public void saveScreencastMode(int mode){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt(SCREENCAST_MODE, mode);

        editor.commit();
    }

    public int getScreencastMode(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getInt(SCREENCAST_MODE, SCREENCAST_MODE_NONE);
    }

    public static final String VALUES_FROM = "VALUES_FROM";
    public static final String VALUE_TO = "VALUE_TO";

    public void saveValueTo(int minutes){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(VALUE_TO, minutes);
        editor.commit();
    }

    public int getValueTo(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getInt(VALUE_TO, 2);
    }

    public void saveValueFrom(int minutes){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(VALUES_FROM, minutes);
        editor.commit();
    }

    public int getValueFrom(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getInt(VALUES_FROM, 1);
    }

    //        exerciseJobBuilder.setMinimumLatency(TimeUnit.SECONDS.toMillis(1));
    //        exerciseJobBuilder.setOverrideDeadline(TimeUnit.SECONDS.toMillis(5));
    //        exerciseJobBuilder.setPeriodic(TimeUnit.SECONDS.toMillis(60));
    //        exerciseJobBuilder.setBackoffCriteria(TimeUnit.SECONDS.toMillis(10), JobInfo.BACKOFF_POLICY_LINEAR);

    //        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    //            exerciseJobBuilder.setMinimumLatency(TimeUnit.SECONDS.toMillis(60));
    //        } else {
    //            exerciseJobBuilder.setPeriodic(1*60*1000);
    //        }
    //        finish();

    public static final int SCREENCAST_SERVICE_ID = 111333;

    public void scheduleJob(Context context) {

        Log.d(getClass().getName(), "private void scheduleJob(Context context) {}");

        int valueFrom = Tools.getInstance().getValueFrom();
        int valueTo = Tools.getInstance().getValueTo();

        Log.d(getClass().getName(), "valueFrom "+String.valueOf(valueFrom));
        Log.d(getClass().getName(), "valueTo " +String.valueOf(valueTo) );

        int periodSeconds = valueFrom * 60 + new Random().nextInt(valueTo*60 );

        Log.d(getClass().getName(), "periodSeconds " + String.valueOf(periodSeconds) );

        ComponentName jobService = new ComponentName(context, ScreencastServiceScheduler.class);
        JobInfo.Builder exerciseJobBuilder = new JobInfo.Builder(SCREENCAST_SERVICE_ID, jobService);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            exerciseJobBuilder.setMinimumLatency(TimeUnit.SECONDS.toMillis(periodSeconds) );
        } else {
            exerciseJobBuilder.setPeriodic( TimeUnit.SECONDS.toMillis(periodSeconds));
        }

        exerciseJobBuilder.setRequiresDeviceIdle(false);
        exerciseJobBuilder.setRequiresCharging(false);

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(exerciseJobBuilder.build());

        Log.d(getClass().getName(), "jobScheduler.schedule(exerciseJobBuilder.build());");

    }



    public void cancelSheduleJob(){

        Log.d(getClass().getName(), "private void cancelSheduleJob(){}");

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(SCREENCAST_SERVICE_ID);

//        finish();
    }

    public static final String LEVEL_NEUTRAL = "level_neutral";
    public static final String LEVEL_SUGGESTIVE = "level_suggestive";
    public static final String LEVEL_EXPLICIT = "level_explicit";

    // Default confidence levels: Explicit - 30%; Suggestive - 40%; Neutral: 5%

    public static final int DEFAULT_LEVEL_EXPLICIT = 30;
    public static final int DEFAULT_LEVEL_SUGGESTIVE = 40;
    public static final int DEFAULT_LEVEL_NEUTRAL = 5;

    public int getExplicitThreshold() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(LEVEL_EXPLICIT, DEFAULT_LEVEL_EXPLICIT);
    }

    public int getSuggestiveThreshold() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(LEVEL_SUGGESTIVE, DEFAULT_LEVEL_SUGGESTIVE);
    }

    public int getNeutralThreshold() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(LEVEL_NEUTRAL, DEFAULT_LEVEL_NEUTRAL);
    }

    public static int getSdkVersion(){
        return android.os.Build.VERSION.SDK_INT;
    }

    public void viewFadeInOut(final View view ){
        Log.d(getClass().getName(), "viewFadeInOut");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int delayTimeSeconds = sharedPref.getInt("screencast_blackout_timeout", 5) - 2;

        AnimatorSet mAnimationSet = new AnimatorSet();

        float fadeValue = 0.8f;
        if(sharedPref.getBoolean("screencast_complete_blackout", false)){
            fadeValue = 1.0f;
        }else{
            fadeValue = 0.8f;
        }

        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, fadeValue);
        fadeIn.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {}

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        fadeIn.setInterpolator(new LinearInterpolator());
        fadeIn.setDuration(1000);

        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(view, View.ALPHA,  fadeValue, 0f);
        fadeOut.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        fadeOut.setInterpolator(new LinearInterpolator());
        fadeOut.setStartDelay(delayTimeSeconds * 1000);
        fadeOut.setDuration(1000);

        mAnimationSet.playSequentially( fadeIn, fadeOut);
        mAnimationSet.start();
    }

    public static final String USE_BLACKOUT = "USE_BLACKOUT";

    public void setUseBlackout(boolean use){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(USE_BLACKOUT, use);
        editor.commit();
    }

    public boolean getUseBlackout(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getBoolean(USE_BLACKOUT, false);
    }

    public static final String SCREENCAST_DEBUG_MODE = "SCREENCAST_DEBUG_MODE";

    public void setScreencastDebugMode(boolean debug){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(SCREENCAST_DEBUG_MODE, debug);
        editor.commit();
    }

    public boolean getScreencastDebugMode(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getBoolean(SCREENCAST_DEBUG_MODE, false);
    }

    public String getNotificationChannelId(){

        String CHANNEL_ID = getClass().getName();

        return CHANNEL_ID;
    }

    public void createNotificationChannel( NotificationManager notificationManager){
        NotificationChannel channel;
        String CHANNEL_ID = getNotificationChannelId();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
             channel = new NotificationChannel( CHANNEL_ID, "Safe surfer", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription("Safe surfer");
            channel.enableLights(false);
            channel.setVibrationPattern(new long[]{ 0 });
            channel.setSound(null, null);
            channel.enableVibration(true);

            notificationManager.createNotificationChannel(channel);
        }
    }

    public void setUserAuthToken(String token){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN, token).commit();
    }

    public String getUserAuthToken(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        return preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN, null);
    }

    public static final int NOTIFICATION_ID = 111555;
    public static final int FOREGROUND_NOTIFICATION_ID = 111333;

    public void setDeviceAuthToken(String token){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, token).commit();
    }

    public String getDeviceAuthToken(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        if(preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null) == null)return null;
        return "Bearer "+preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null);
    }

    public String getClearedDeviceAuthToken(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        if(preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null) == null)return null;
        return preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null);
    }

    public void setCurrentScreencastId(String id){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_SCREENCAST_ID, id).commit();
    }

    public String getCurrentScreencastId(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        return preferences.getString(REGISTRATION_PREFERENCES_KEY_SCREENCAST_ID, null);
    }

    public void showKeyboard(EditText editableView){
        InputMethodManager keyboard = (InputMethodManager)editableView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.showSoftInput(editableView, 0);
    }

    public void hideKeyboard(Context context){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = ((AppCompatActivity)context).getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View( ((AppCompatActivity)context));
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static final String USER_AGREE_TERMS = "USER_AGREE_TERMS";

    public void setUserAgreeTerms(boolean value){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putBoolean(USER_AGREE_TERMS, value).commit();
    }

    public boolean getUserAgreeTerms(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        return preferences.getBoolean(USER_AGREE_TERMS, false);
    }

    public static final String CURRENT_USER_LOGIN= "CURRENT_USER_LOGIN";

    public void saveCurrentUserLogin(String userLogin){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(CURRENT_USER_LOGIN, userLogin).commit();
    }
    public String getCurrentUserLogin(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        return preferences.getString(CURRENT_USER_LOGIN, "");
    }

    public String getDeviceId(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);

        return preferences.getString(REGISTRATION_PREFERENCES_KEY_DEVICEID, "");
    }

    public static String removeQuotes(String input){
        return input.replaceAll("^\"|\"$", "");
    }

    public static final int JWT_TOKEN_TTL_SECONDS = 7890000;
    public static final Long JWT_TOKEN_TTL_MILLISECONDS = 7890000000l;
}
