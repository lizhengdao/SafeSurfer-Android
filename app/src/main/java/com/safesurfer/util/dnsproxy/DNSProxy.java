package com.safesurfer.util.dnsproxy;

import android.net.VpnService;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import androidx.annotation.RequiresApi;
import com.safesurfer.LogFactory;
import com.safesurfer.database.entities.IPPortPair;
import com.safesurfer.util.PreferencesAccessor;

import android.system.ErrnoException;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Set;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public abstract class DNSProxy {
    private static final String LOG_TAG = "[DNSPROXY]";
    public static final String IPV4_LOOPBACK_REPLACEMENT = "1.0.0.0",
    IPV6_LOOPBACK_REPLACEMENT = "fdce:b45b:8dd7:6e47:1:2:3:4";
    static InetAddress LOOPBACK_IPV4, LOOPBACK_IPV6;
    static{
        try {
            LOOPBACK_IPV4 = Inet4Address.getByName("127.0.0.1");
            LOOPBACK_IPV6 = Inet6Address.getByName("::1");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public abstract void run() throws InterruptedException, IOException, ErrnoException;

    public abstract void stop();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static DNSProxy createProxy(VpnService context, ParcelFileDescriptor parcelFileDescriptor,
                                       Set<IPPortPair> upstreamDNSServers, boolean resolveLocalRules, boolean queryLogging, boolean logUpstreamAnswers) {
        LogFactory.writeMessage(context, LOG_TAG, "Creating a proxy with upstreamservers: " + upstreamDNSServers + " and file descriptor: " + parcelFileDescriptor);
        if (PreferencesAccessor.sendDNSOverTCP(context)) {
            LogFactory.writeMessage(context, LOG_TAG, "Creating a TCP proxy");
            return new DNSTCPProxy(context, parcelFileDescriptor, upstreamDNSServers,
                    resolveLocalRules, queryLogging, logUpstreamAnswers, PreferencesAccessor.getTCPTimeout(context));
        } else if(PreferencesAccessor.sendDNSOverTLS(context)){
            LogFactory.writeMessage(context, LOG_TAG, "Creating a TLS proxy");
            return new DNSTLSProxy(context, parcelFileDescriptor, upstreamDNSServers,
                    resolveLocalRules, queryLogging, logUpstreamAnswers, PreferencesAccessor.getTCPTimeout(context));
        } else {
            LogFactory.writeMessage(context, LOG_TAG, "Creating an UDP proxy");
            return new DNSUDPProxy(context, parcelFileDescriptor, upstreamDNSServers,
                    resolveLocalRules, queryLogging, logUpstreamAnswers);
        }
    }
}
