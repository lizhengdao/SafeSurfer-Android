package com.safesurfer.tensorflow;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.safesurfer.R;
import com.safesurfer.tensorflow.tflite.Classifier;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerActivity;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TestTensorFlowRecognitionActivity extends AppCompatActivity {

    public static final int SCREENCAPTURER_SERVICE_ID = 100500;

    private static final String MODEL_PATH = "neutral-porn-suggestive-classifier.tflite";
    private static final boolean QUANT = false;
    private static final String LABEL_PATH = "labels.txt";
    private static final int INPUT_SIZE = 224;

    private Classifier classifier;

    private Executor executor = Executors.newSingleThreadExecutor();
    private TextView textViewResult;
    private Button btnDetectObject, btnToggleCamera;

    private ImageView imageView;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_test_tensor_flow_recognition);

//        initTensorFlowAndLoadModel();

        imageView = findViewById(R.id.imageview);

        findViewById(R.id.take_screenshot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ScreenCapturerActivity.class);
                startActivity(intent);

//                scheduleJob(context);
            }
        });

        findViewById(R.id.cancel_take_screenshot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(context, ScreenCapturerActivity.class);
//                startActivity(intent);

//                cancelSheduleJob();
            }
        });

        if(android.os.Build.VERSION.SDK_INT >= 23){
            requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            },100500);
        }
    }

    Handler mUIHandler = new Handler();

    @Override
    protected void onResume() {
        super.onResume();


    }

    //        mUIHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                /*
//                Bitmap bitmap = cameraKitImage.getBitmap();
//
//                bitmap = Bitmap.createScaledBitmap(bitmap, INPUT_SIZE, INPUT_SIZE, false);
//
//                imageViewResult.setImageBitmap(bitmap);
//
//                final List<Classifier.Recognition> results = classifier.recognizeImage(bitmap);
//
//                textViewResult.setText(results.toString());
//                */
//
//                Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
//
//                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, INPUT_SIZE, INPUT_SIZE, false);
//
//                final List<Classifier.Recognition> results = classifier.recognizeImage(scaledBitmap);
//
//                Log.d(getClass().getName(), results.toString() );
//
//            }
//        }, 3 * 1000);

//    private void initTensorFlowAndLoadModel() {
//        executor.execute(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    classifier = TensorFlowImageClassifier.create(
//                            getAssets(),
//                            MODEL_PATH,
//                            LABEL_PATH,
//                            INPUT_SIZE,
//                            QUANT);
//
//                } catch (final Exception e) {
//                    throw new RuntimeException("Error initializing TensorFlow!", e);
//                }
//            }
//        });
//    }

//    private void scheduleJob(Context context) {
//        try {
//            ComponentName jobService = new ComponentName(context, ScreenCapturerServiceScheduler.class);
//            JobInfo.Builder exerciseJobBuilder = new JobInfo.Builder(SCREENCAPTURER_SERVICE_ID, jobService);
//
//            exerciseJobBuilder.setMinimumLatency(TimeUnit.SECONDS.toMillis(60));
//
//            exerciseJobBuilder.setRequiresDeviceIdle(false);
//            exerciseJobBuilder.setRequiresCharging(false);
//
//            Log.d(getClass().getName(), "scheduleJob: adding job to scheduler");
//
//            JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//            jobScheduler.schedule(exerciseJobBuilder.build());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        finish();
//    }
//
//    private void cancelSheduleJob(){
//        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        jobScheduler.cancel(SCREENCAPTURER_SERVICE_ID);
//
//        finish();
//    }

}
