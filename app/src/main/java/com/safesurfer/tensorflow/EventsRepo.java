package com.safesurfer.tensorflow;

import android.content.Context;

import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;

public class EventsRepo {

    private Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public EventsRepo(Context context) {
        setContext(context);
    }

    public EventModelScores getNewEventModelScoreIdentifier(){

        Realm realm = Realm.getDefaultInstance();
        EventModelScores model;

        int newId;
        if(realm.where(EventModelScores.class).max("identifier") == null){
            newId = 2000;
        }else{
            newId = realm.where(EventModelScores.class).max("identifier").intValue() + 1;
        }
        model = realm.createObject(EventModelScores.class, Integer.valueOf(newId) );

        return model;
    }

    public int getNewEventScoreModel(){
        int newId;
        Realm realm = Realm.getDefaultInstance();
        if(realm.where(EventModelScores.class).max("identifier") == null){
            newId = new Random().nextInt();
        }else{
            newId = realm.where(EventModelScores.class).max("identifier").intValue() + 1;
        }

        return newId;
    }

    public int getNewEventIdentifier(){
        int newId;
        Realm realm = Realm.getDefaultInstance();
        if(realm.where(EventModel.class).max("identifier") == null){
            newId = new Random().nextInt();
        }else{
            newId = realm.where(EventModel.class).max("identifier").intValue() + 1;
        }

        return newId;
    }

    public EventModel getNewEventModel(){

        Realm realm = Realm.getDefaultInstance();

        int newId;
        if(realm.where(EventModel.class).max("identifier") == null){
            newId = 1000;
        }else{
            newId = realm.where(EventModel.class).max("identifier").intValue() + 1;
        }
        EventModel model = realm.createObject(EventModel.class, Integer.valueOf(newId) );

        return model;
    }


    public EventModel getEventModel(int identifier) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<EventModel> list = realm
                .where(EventModel.class)
                .equalTo("identifier",identifier)
                .findAll();
        if(list.size() > 0){
            return list.get(0);
        }

        return null;
    }

    public void dropEventModelsList() {

        Realm realm = Realm.getDefaultInstance();
        RealmResults<EventModel> list = realm.where(EventModel.class).findAll();
        realm.beginTransaction();
        list.deleteAllFromRealm();
        realm.commitTransaction();
    }

}
