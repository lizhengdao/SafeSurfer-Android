package com.safesurfer.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.OrientationHelper;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.frostnerd.design.dialogs.LoadingDialog;
import com.safesurfer.LogFactory;
import com.safesurfer.R;
import com.safesurfer.screens.MainActivity;
import com.safesurfer.database.entities.IPPortPair;
import com.safesurfer.dialogs.VPNInfoDialog;
import com.safesurfer.services.DNSVpnService;
import com.safesurfer.util.DNSQueryUtil;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.ThemeHandler;
import com.safesurfer.util.Util;
import com.safesurfer.util.Preferences;
import com.frostnerd.general.Utils;

import java.util.ArrayList;
import java.util.List;

import de.measite.minidns.Record;
import de.measite.minidns.record.Data;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class MainFragment extends Fragment implements View.OnClickListener {
    private CardView startStopButton;

    public static final String DEVICE_IS_PROTECTED = "BR_DEVICE_IS_PROTECTED";
    public static final String DEVICE_IS_PROTECTED_VALUE = "DEVICE_IS_PROTECTED_VALUE";


    IntentFilter filter = new IntentFilter();
    BroadcastReceiver reciever;


    private CardView mStartProtection;
    private CardView mStopProtection;
    private boolean vpnRunning, wasStartedWithTasker = false;

    TextView mProtectionLabel;
    private RelativeLayout mProtectedLayout;
    private RelativeLayout mNotProtectedLayout;
    private static final String LOG_TAG = "[MainActivity]";
    private TextView connectionText;
    private ImageView connectionImage;
    private View running_indicator;
    private boolean advancedMode;
    public boolean settingV6 = false;
    private final BroadcastReceiver serviceStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogFactory.writeMessage(requireContext(), LOG_TAG, "Received ServiceState Answer", intent);
            vpnRunning = intent.getBooleanExtra("vpn_running",false);
            wasStartedWithTasker = intent.getBooleanExtra("started_with_tasker", false);
            setIndicatorState(intent.getBooleanExtra("vpn_running",false));
        }
    };
    private View contentView;
    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sp, String s) {
            if(s.equals("everything_disabled")){
                boolean value = Preferences.getInstance(requireContext()).getBoolean("everything_disabled", false);
               /* startStopButton.setEnabled(!value);
                startStopButton.setClickable(!value);
                startStopButton.setAlpha(value ? 0.50f : 1f);*/


                mStartProtection.setEnabled(!value);
                mStartProtection.setClickable(!value);
                mStartProtection.setAlpha(value ? 0.50f : 1f);

                mStopProtection.setEnabled(!value);
                mStopProtection.setClickable(!value);
                mStopProtection.setAlpha(value ? 0.50f : 1f);
                if(!value) setIndicatorState(vpnRunning);
            }
        }
    };

    private void setIndicatorState(boolean vpnRunning) {
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Changing IndicatorState to " + vpnRunning);
        if (vpnRunning) {
            connectionText.setText(R.string.running);
            if(connectionImage != null)connectionImage.setImageResource(R.drawable.ic_thumb_up);
          /*  mProtectedLayout.setVisibility(View.VISIBLE);
            mNotProtectedLayout.setVisibility(View.GONE);*/
            //mProtectionLabel.setText(R.string.stop);
            running_indicator.setBackgroundColor(Color.parseColor("#4CAF50"));
        } else {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = requireContext().getTheme();
            theme.resolveAttribute(android.R.attr.windowBackground, typedValue, true);
            if(PreferencesAccessor.isEverythingDisabled(requireContext()))  connectionText.setText(R.string.info_functionality_disabled);
            else connectionText.setText(R.string.not_running);
            if(connectionImage != null)connectionImage.setImageResource(R.drawable.ic_thumb_down);
           /* mProtectedLayout.setVisibility(View.GONE);
            mNotProtectedLayout.setVisibility(View.VISIBLE);*/
            //mProtectionLabel.setText(R.string.start);
            running_indicator.setBackgroundColor(typedValue.data);
        }
        LogFactory.writeMessage(requireContext(), LOG_TAG, "IndictorState set");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.fragment_main, container, false);
        return contentView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        startStopButton = null;

        mStopProtection = null;
        mStartProtection = null;

        connectionText = null;
        connectionImage = null;
        running_indicator = null;
        contentView = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            settingV6 = !PreferencesAccessor.isIPv4Enabled(requireContext()) || (PreferencesAccessor.isIPv6Enabled(requireContext()) && settingV6);
            setHasOptionsMenu(true);
            boolean vertical = getResources().getConfiguration().orientation == OrientationHelper.VERTICAL;
            LogFactory.writeMessage(requireContext(), LOG_TAG, "Created Activity", Util.getActivity(this).getIntent());
            LogFactory.writeMessage(requireContext(), LOG_TAG, "Setting ContentView");


            mProtectedLayout = (RelativeLayout) findViewById(R.id.protected_layout);
            mNotProtectedLayout = (RelativeLayout) findViewById(R.id.not_protected_layout);
            //mProtectionLabel = (TextView) findViewById(R.id.protection_label);
            connectionImage = vertical ? null : (ImageView) findViewById(R.id.connection_status_image);
            connectionText = (TextView) findViewById(R.id.connection_status_text);
            running_indicator = findViewById(R.id.running_indicator);
            //startStopButton = (CardView) findViewById(R.id.startStopButton);
            mStartProtection = (CardView) findViewById(R.id.start_protection);
            mStopProtection = (CardView) findViewById(R.id.stop_button);

            mStartProtection.setOnClickListener(this);

            mStopProtection.setOnClickListener(this);
        }
        catch (Exception ex)
        {
            LogFactory.writeMessage(requireContext(),LOG_TAG,ex.toString());
        }
        mStartProtection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProtectedLayout.setVisibility(View.VISIBLE);
                mNotProtectedLayout.setVisibility(View.GONE);
                final Intent i = VpnService.prepare(requireContext());
                LogFactory.writeMessage(requireContext(), LOG_TAG, "Startbutton clicked. Configuring VPN if needed");
                if (i != null) {
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "VPN isn't prepared yet. Showing dialog explaining the VPN");
                    new VPNInfoDialog(requireContext(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            startActivityForResult(i, 0);
                            LogFactory.writeMessage(requireContext(), LOG_TAG, "Requesting VPN access", i);
                        }
                    });
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Dialog is now being shown");
                } else {
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "VPNService is already configured");
                    onActivityResult(0, Activity.RESULT_OK, null);
                }
            }
        });

        mStopProtection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProtectedLayout.setVisibility(View.GONE);
                mNotProtectedLayout.setVisibility(View.VISIBLE);
                final Intent i = VpnService.prepare(requireContext());
                LogFactory.writeMessage(requireContext(), LOG_TAG, "Startbutton clicked. Configuring VPN if needed");
                if (i != null) {
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "VPN isn't prepared yet. Showing dialog explaining the VPN");
                    new VPNInfoDialog(requireContext(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            startActivityForResult(i, 0);
                            LogFactory.writeMessage(requireContext(), LOG_TAG, "Requesting VPN access", i);
                        }
                    });
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Dialog is now being shown");
                } else {
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "VPNService is already configured");
                    onActivityResult(0, Activity.RESULT_OK, null);
                }
            }
        });

    }

    private View findViewById(@IdRes int id){
        return contentView.findViewById(id);
    }

    @Override
    public void onResume() {
        super.onResume();
        advancedMode = PreferencesAccessor.isRunningInAdvancedMode(requireContext());
        Preferences.getDefaultPreferences(requireContext()).registerOnSharedPreferenceChangeListener(preferenceChangeListener);
        settingV6 = !PreferencesAccessor.isIPv4Enabled(requireContext()) || (PreferencesAccessor.isIPv6Enabled(requireContext()) && settingV6);
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Got OnResume");
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Sending ServiceStateRequest as broadcast");
        vpnRunning = Util.isServiceRunning(requireContext());
        if(PreferencesAccessor.isEverythingDisabled(requireContext())){

            mStartProtection.setEnabled(false);
            mStartProtection.setClickable(false);
            mStartProtection.setAlpha(0.50f);

            mStopProtection.setEnabled(false);
            mStopProtection.setClickable(false);
            mStopProtection.setAlpha(0.50f);

            mProtectedLayout.setVisibility(View.GONE);
            mNotProtectedLayout.setVisibility(View.VISIBLE);
            mProtectionLabel.setText(R.string.start);



          /*  startStopButton.setEnabled(false);
            startStopButton.setClickable(false);
            startStopButton.setAlpha(0.50f);*/
            connectionText.setText(R.string.info_functionality_disabled);
        }else{
           /* startStopButton.setEnabled(true);
            startStopButton.setClickable(true);
            startStopButton.setAlpha(1f);*/
            try {
                mStartProtection.setEnabled(true);
                mStartProtection.setClickable(true);
                mStartProtection.setAlpha(1f);

                mStopProtection.setEnabled(true);
                mStopProtection.setClickable(true);
                mStopProtection.setAlpha(1f);
                if (vpnRunning == true) {
                    mProtectedLayout.setVisibility(View.VISIBLE);
                    mNotProtectedLayout.setVisibility(View.GONE);
                    //mProtectionLabel.setText(R.string.stop);
                }
                else
                {
                    mProtectedLayout.setVisibility(View.GONE);
                    mNotProtectedLayout.setVisibility(View.VISIBLE);
                }


                setIndicatorState(vpnRunning);
            }
             catch (Exception ex)
            {
                LogFactory.writeMessage(requireContext(), LOG_TAG, ex.toString());
            }
        }
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(serviceStateReceiver, new IntentFilter(Util.BROADCAST_SERVICE_STATUS_CHANGE));
        LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(new Intent(Util.BROADCAST_SERVICE_STATE_REQUEST));
        //Utils.requireNonNull(((AppCompatActivity)requireContext()).getSupportActionBar()).setSubtitle(getString(R.string.subtitle_configuring).replace("[[x]]",settingV6 ? "Ipv6" : "Ipv4"));
        Utils.requireNonNull(Util.getActivity(this)).invalidateOptionsMenu();
        Utils.requireNonNull(Util.getActivity(this)).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //registerReceiver(shortcutReceiver, new IntentFilter(Util.BROADCAST_SHORTCUT_CREATED));
        filter.addAction(DEVICE_IS_PROTECTED);
        reciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(DEVICE_IS_PROTECTED)){
                    if(intent.getBundleExtra("extra") == null)return;
                    boolean isProtected = intent.getBundleExtra("extra").getBoolean(DEVICE_IS_PROTECTED_VALUE);
                    if(getView() == null)return;
                    if(isProtected){
                        getView().findViewById(R.id.protected_layout).setVisibility(View.VISIBLE);
                        getView().findViewById(R.id.not_protected_layout).setVisibility(View.GONE);
                    }else{
                        getView().findViewById(R.id.protected_layout).setVisibility(View.GONE);
                        getView().findViewById(R.id.not_protected_layout).setVisibility(View.VISIBLE);
                    }
                }
            }
        };
        getActivity().registerReceiver(reciever, filter);
    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(reciever);

        LogFactory.writeMessage(requireContext(), LOG_TAG, "Got OnPause");
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(serviceStateReceiver);
        Preferences.getDefaultPreferences(requireContext()).unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Got OnActivityResult" ,data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            if (!vpnRunning){
                if(!Preferences.getInstance(requireContext()).getBoolean("44explained", false) && Build.VERSION.SDK_INT == 19){
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Opening Dialog explaining that this might not work on Android 4.4");
                    new AlertDialog.Builder(requireContext(), ThemeHandler.getDialogTheme(requireContext())).setTitle(R.string.warning).setCancelable(false).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            startVpn();
                        }
                    }).setMessage(R.string.android4_4_warning).show();
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Dialog is now being shown");
                }else{
                    startVpn();
                }
                Preferences.getInstance(requireContext()).getBoolean("44explained", true);
            }else{
                if(wasStartedWithTasker){
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Opening dialog which warns that the app was started using Tasker");
                    new AlertDialog.Builder(requireContext(),ThemeHandler.getDialogTheme(requireContext())).setTitle(R.string.warning).setMessage(R.string.warning_started_using_tasker). setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            LogFactory.writeMessage(requireContext(), LOG_TAG, "User clicked OK in the dialog warning about Tasker");
                            stopVpn();
                            dialog.cancel();
                        }
                    }).setCancelable(false).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            LogFactory.writeMessage(requireContext(), LOG_TAG, "User cancelled stopping DNSChanger as it was started using tasker");
                        }
                    }).show();
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Dialog is now being shown");
                }else stopVpn();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void startVpn() {
        if(PreferencesAccessor.checkConnectivityOnStart(requireContext())){
            final LoadingDialog dialog = new LoadingDialog(requireContext(), R.string.checking_connectivity, R.string.dialog_connectivity_description);
            dialog.show();
            checkDNSReachability(new DNSReachabilityCallback() {
                @Override
                public void checkFinished(@NonNull List<IPPortPair> unreachable, @NonNull List<IPPortPair> reachable) {
                    dialog.dismiss();
                    if(unreachable.size() == 0){
                        ((MainActivity)requireContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                start();
                            }
                        });
                    }else{
                        String _text = getString(R.string.no_connectivity_warning_text);
                        StringBuilder builder = new StringBuilder();
                        _text = _text.replace("[x]", unreachable.size() + reachable.size() + "");
                        _text = _text.replace("[y]", unreachable.size() + "");
                        boolean customPorts = PreferencesAccessor.areCustomPortsEnabled(requireContext());
                        for(IPPortPair p: unreachable) {
                            if(p == null)continue;
                            builder.append("- ").append(p.formatForTextfield(customPorts)).append("\n");
                        }
                        _text = _text.replace("[servers]", builder.toString());
                        final String text = _text;
                        ((MainActivity)requireContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(requireContext(), ThemeHandler.getDialogTheme(requireContext()))
                                        .setTitle(R.string.warning).setCancelable(true).setPositiveButton(R.string.start, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        start();
                                    }
                                }).setNegativeButton(R.string.cancel, null).setMessage(text).show();
                            }
                        });
                    }
                }

                private void start(){
                    Intent i;
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Starting VPN",
                            i = DNSVpnService.getStartVPNIntent(requireContext()));
                    wasStartedWithTasker = false;
                    Util.startService(requireContext(), i);
                    vpnRunning = true;
                    setIndicatorState(true);
                }
            });
        }else{
            Intent i;
            LogFactory.writeMessage(requireContext(), LOG_TAG, "Starting VPN",
                    i = DNSVpnService.getStartVPNIntent(requireContext()));
            wasStartedWithTasker = false;
            Util.startService(requireContext(), i);
            vpnRunning = true;
            setIndicatorState(true);
        }
    }

    private void stopVpn() {
        Intent i;
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Stopping VPN",
                i = DNSVpnService.getDestroyIntent(requireContext()));
        requireContext().startService(i);
        vpnRunning = false;
        setIndicatorState(false);
    }

    public void toggleVPN(){
        if (vpnRunning){
            stopVpn();
        }else startVpn();
    }


    public void checkDNSReachability(final DNSReachabilityCallback callback){
        List<IPPortPair> servers = PreferencesAccessor.getAllDNSPairs(requireContext(), true);
        callback.setServers(servers.size());
        for(final IPPortPair pair: servers){
            DNSQueryUtil.runAsyncDNSQuery(pair, "frostnerd.com", PreferencesAccessor.sendDNSOverTCP(requireContext()), Record.TYPE.A,
                    Record.CLASS.IN, new Util.DNSQueryResultListener() {
                @Override
                public void onSuccess(List<Record<? extends Data>> response) {
                    callback.checkProgress(pair, true);
                }

                @Override
                public void onError(@Nullable Exception e) {
                    callback.checkProgress(pair, false);
                }
            }, 1);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(PreferencesAccessor.isIPv6Enabled(requireContext()) ? (PreferencesAccessor.isIPv4Enabled(requireContext()) ? ((settingV6 ? R.menu.menu_main_v6 : R.menu.menu_main)) : R.menu.menu_main_no_ipv6) : R.menu.menu_main_no_ipv6,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_switch_ip_version){
            settingV6 = !settingV6;
            Util.getActivity(this).invalidateOptionsMenu();
            ((AppCompatActivity)requireContext()).getSupportActionBar().setSubtitle(getString(R.string.subtitle_configuring).replace("[[x]]",settingV6 ? "Ipv6" : "Ipv4"));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        final Intent i = VpnService.prepare(requireContext());
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Startbutton clicked. Configuring VPN if needed");
        if (i != null) {
            LogFactory.writeMessage(requireContext(), LOG_TAG, "VPN isn't prepared yet. Showing dialog explaining the VPN");
            new VPNInfoDialog(requireContext(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    startActivityForResult(i, 0);
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Requesting VPN access", i);
                }
            });
            LogFactory.writeMessage(requireContext(), LOG_TAG, "Dialog is now being shown");
        } else {
            LogFactory.writeMessage(requireContext(), LOG_TAG, "VPNService is already configured");
            onActivityResult(0, Activity.RESULT_OK, null);
        }
    }

    public static abstract class DNSReachabilityCallback{
        @NonNull private final List<IPPortPair> unreachable = new ArrayList<>();
        @NonNull private final List<IPPortPair> reachable = new ArrayList<>();
        private int servers;

        public abstract void checkFinished(@NonNull List<IPPortPair> unreachable, @NonNull List<IPPortPair> reachable);

        public final void checkProgress(@NonNull IPPortPair server, boolean reachable){
            if(server == null || server.isEmpty())return;
            if(!reachable)unreachable.add(server);
            else this.reachable.add(server);
            if(this.unreachable.size() + this.reachable.size() >= servers)checkFinished(this.unreachable, this.reachable);
        }

        void setServers(int servers){
            this.servers = servers;
        }

    }
}
