package com.safesurfer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.VpnService;
import android.util.Log;

import com.safesurfer.LogFactory;
import com.safesurfer.screens.BackgroundVpnConfigureActivity;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerActivity;
import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;
import com.safesurfer.util.Preferences;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 * <p>
 * This file is part of SafeSurfer-Android.
 * <p>
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class BootReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = "[BootReceiver]";

    @Override
    public void onReceive(Context context, Intent intent) {
        LogFactory.writeMessage(context, LOG_TAG, "Received an intent ", intent);
        if (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED) || intent.getAction().equals(Intent.ACTION_LOCKED_BOOT_COMPLETED)) {

            Log.d(getClass().getName(), "if(intent.getAction() != null && intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED) || intent.getAction().equals(Intent.ACTION_LOCKED_BOOT_COMPLETED)){}");

            LogFactory.writeMessage(context, LOG_TAG, "Action is BOOT_COMPLETED");

            Tools.getInstance().init(context);
            if (Tools.getInstance().isScreencastDetectionActive()) {

                Tools.getInstance().setScreencastDetectionActive(true);
                Tools.getInstance().saveScreencastMode(Tools.SCREENCAST_MODE_WATCHING);

                Intent __intent = new Intent(context, ScreenCapturerActivity.class);

                __intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(__intent);

            }


            LogFactory.writeMessage(context, LOG_TAG, "Starting ConnectivityBackgroundService");
            Util.runBackgroundConnectivityCheck(context, false);
            Preferences.getInstance(context).put("everything_disabled", false);
            if (Preferences.getInstance(context).getBoolean("setting_start_boot", false)) {
                LogFactory.writeMessage(context, LOG_TAG, "User wants App to start on boot");
                Intent i = VpnService.prepare(context);
                LogFactory.writeMessage(context, LOG_TAG, "VPNService Prepare Intent", i);
                BackgroundVpnConfigureActivity.startBackgroundConfigure(context, true);
            }

        }
    }
}
