package com.safesurfer.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.frostnerd.database.CursorWithDefaults;
import com.frostnerd.database.orm.Entity;

import com.safesurfer.database.entities.DNSQuery;
import com.safesurfer.database.entities.DNSTLSConfiguration;
import com.safesurfer.database.entities.IPPortPair;
import com.safesurfer.database.entities.Shortcut;
import com.frostnerd.general.Utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class DatabaseHelper extends com.frostnerd.database.DatabaseHelper {
    public static final String DATABASE_NAME = "data";
    public static final int DATABASE_VERSION = 4;
    @NonNull
    public static final Set<Class<? extends Entity>> entities = new HashSet<Class<? extends Entity>>(){{
        add(DNSQuery.class);
        add(IPPortPair.class);
        add(Shortcut.class);
        add(DNSTLSConfiguration.class);
    }};
    @Nullable
    private static DatabaseHelper instance;
    @NonNull
    private Context wrappedContext;

    public static DatabaseHelper getInstance(@NonNull Context context){
        return instance == null ? instance = new DatabaseHelper(context.getApplicationContext() != null ? context.getApplicationContext() : context) : instance;
    }

    public static boolean instanceActive(){
        return instance != null;
    }

    private DatabaseHelper(@NonNull Context context) {
        super(context, DATABASE_NAME, DATABASE_VERSION, entities);
        wrappedContext = context;
    }

    @Override
    public void onAfterCreate(SQLiteDatabase db) {
    }

    @Override
    public void onBeforeCreate(SQLiteDatabase db) {

    }

    @Override
    public void onBeforeUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private String legacyString(@NonNull String column, @NonNull CursorWithDefaults cursor, @NonNull String defaultValue){
        return legacyString(cursor.getColumnIndex(column), cursor, defaultValue);
    }

    private String legacyString(int column, @NonNull CursorWithDefaults cursor, @NonNull String defaultValue){
        String fromDB = cursor.getStringNonEmpty(column, defaultValue);
        fromDB = fromDB.replaceAll("\\r|\\n", "");
        return Utils.notEmptyOrDefault(fromDB, defaultValue);
    }

    @Override
    public void onAfterUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public synchronized void close() {
        instance = null;
        wrappedContext = null;
        super.close();
    }

    @Nullable
    public DNSTLSConfiguration findTLSConfiguration(@NonNull IPPortPair pair){
        for(DNSTLSConfiguration configuration: getAll(DNSTLSConfiguration.class)){
            for(IPPortPair ip: configuration.getAffectedServers()) {
                if(ip.getAddress().equalsIgnoreCase(pair.getAddress())) return configuration;
            }
        }
        return null;
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }
}
