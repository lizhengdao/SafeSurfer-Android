package com.safesurfer.database.entities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.frostnerd.database.orm.MultitonEntity;
import com.frostnerd.database.orm.annotations.Named;
import com.frostnerd.database.orm.annotations.RowID;
import com.frostnerd.database.orm.annotations.Serialized;
import com.frostnerd.database.orm.annotations.Table;
import com.safesurfer.database.serializers.IPPortSerializer;

import java.util.HashSet;
import java.util.Set;

import lombok.NoArgsConstructor;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
@NoArgsConstructor
@Table(name = "DNSTLSConfiguration")
public class DNSTLSConfiguration extends MultitonEntity {
    @Named(name = "id")
    @RowID
    private long ID;
    @Named(name = "port")
    private int port;
    @Named(name = "host")
    @Nullable
    private String hostName;
    @Serialized(scope = Serialized.Scope.INNER, using = IPPortSerializer.class)
    @NonNull
    @Named(name = "affected_servers")
    private HashSet<IPPortPair> affectedServers;

    public DNSTLSConfiguration(int port, @NonNull HashSet<IPPortPair> affectedServers) {
        this(port, affectedServers, null);
    }

    public DNSTLSConfiguration(int port, @NonNull HashSet<IPPortPair> affectedServers, @Nullable String hostName) {
        this.port = port;
        this.hostName = hostName;
        this.affectedServers = affectedServers;
    }

    public int getPort() {
        return port;
    }

    public long getID() {
        return ID;
    }

    public String getHostName() {
        return hostName;
    }

    @NonNull
    public Set<IPPortPair> getAffectedServers() {
        return affectedServers;
    }

    @Override
    public String toString() {
        return "DNSTLSConfiguration{" +
                "ID=" + ID +
                ", port=" + port +
                ", hostName='" + hostName + '\'' +
                ", affectedServers=" + affectedServers +
                '}';
    }
}
