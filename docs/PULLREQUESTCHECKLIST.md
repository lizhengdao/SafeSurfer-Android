# Pull request checklist
Thank you for wanting to contribute your changes to this project!  
Here are a short list of things to check before submitting a pull request.  

## General
- [ ] Do the changes fit in to the project?  
- [ ] Are the changes useful, helpful, and/or a good idea?  

## Code
- [ ] Is your code consistent with the project -- Do all areas of the project appear to be written in a similar way?  
- [ ] Does your code not break anything else in the project?

## Translation
- [ ] Is your translation concise yet keep it's meaning?  