# Building

## Build dependencies
- Android studio

## Build process
### Android Studio
Use the play button to build and run the app.

### Command line
Linux or macOS:
```bash
./gradlew build --refresh-dependencies
```

Windows:
```cmd
gradlew build --refresh-dependencies
```

## Crashlytics
Add the API key to `app/src/main/AndroidManifest.xml` in the value of `io.fabric.ApiKey`.
Add the API secret to `app/fabric.properties` in the value `apiSecret`.