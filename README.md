<a href="http://www.gnu.org/licenses/gpl-3.0.html">
    <img src="https://img.shields.io/badge/License-GPL%20v3-blue.svg" alt="License" />
</a>
<a href="https://play.google.com/store/apps/details?id=com.safesurfer&hl=en">
    <img src="https://img.shields.io/badge/version-2.0.0-brightgreen.svg" />
</a>
<a href="https://play.google.com/store/apps/details?id=com.safesurfer&hl=en">
    <img src="https://img.shields.io/badge/build-46-orange.svg" />
</a>

![Safe Surfer logo](./app/src/main/res/mipmap-xhdpi/ic_launcher.png)
# SafeSurfer-Android
Official Safe Surfer Android app.

<img width="350px" src="./screenshots/SafeSurfer-Android-Activated.png" />

## Who is [Safe Surfer](http://safesurfer.co.nz)?
Safe Surfer's mission is to keep children, individuals, and organisations safe online. We achieve this by filtering out harmful material that may be found when browsing the internet and switching on safe search for a number of search engines.  
You can read more [here](http://www.safesurfer.co.nz/the-cause).  

## App information
Safe Surfer's Android app sets Safe Surfer up on your device for you.  
Our aim for this project is to make it as easy as possible to get families and persons protected online on their Android devices, ensuring safety and peace-of-mind.  

For enterprise/business use, we recommend to apply the DNS settings on a router which devices are connected to, please refer to the [Safe Surfer website](http://safesurfer.co.nz).  

## Download
You can download the app from [Google Play](https://play.google.com/store/apps/details?id=com.safesurfer) (F-Droid coming soon)

## Features
- Toggle DNS settings with on button
- Protects against harmful content
- Easy to use and setup
- Passcode app locking

## Feedback
Have you used our Android app and want to give feedback?
Visit our [feedback site](http://safesurfer.co.nz/feedback) or [Google Play](https://play.google.com/store/apps/details?id=com.safesurfer) to leave us some feedback.

## Building
Consult the [build manual](docs/BUILDING.md) for instructions on building and running from source.

## Bugs
Wanna help us find and squash bugs?
Check out our [bug reporting guide](docs/BUGS.md).

## Contributing
Read our [contribution guide](docs/CONTRIBUTING.md) to get started!
We'd love your help improving this project, together helping families and individuals stay safe on the internet!

## System requirements
This app is compatible with Android versions 5.0 and up.  
Requires minimal specifications to run.  

## Credits
Thanks to Daniel Wolf for allowing Safe Surfer to use his projects [DnsChanger](https://git.frostnerd.com/PublicAndroidApps/DnsChanger) as the base of this project.  
Thanks to Darren Twiss for contributing to versions 1.0 to 1.8.

## License
Copyright 2019 Safe Surfer.   
Copyright 2017-2019 Daniel Wolf.   
This project is licensed under the [GPL-3.0](http://www.gnu.org/licenses/gpl-3.0.html) and is [Free Software](https://www.gnu.org/philosophy/free-sw.en.html).  
This program comes with absolutely no warranty.
